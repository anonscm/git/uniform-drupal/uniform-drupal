<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="xsl" version="1.0">
	<xsl:output encoding="UTF-8" indent="yes" media-type="text/html" method="xml" omit-xml-declaration="yes" xml:lang="fr" xml:space="preserve"/>
	<xsl:param name="_pageIG"/>
	<!-- FICHIERS INCLUDE UTILISEES   -->
	<!--INCLUDE OF '../commun/composants/commun/commun.xsl'--><xsl:key xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="objet" match="orgUnit | program | subProgram | course | person" use="@id"/><xsl:key xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="noeud" match="noeud" use="@nid"/><xsl:key xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="noeudId" match="noeud" use="@id"/><xsl:key xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="noeudType" match="noeud" use="@typeObjet"/><xsl:key xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="noeudNiveau" match="noeud" use="@niveau"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_lang"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="codeFormation"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="facet"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="baseActionURL"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="absActionURL"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_pas">10</xsl:param><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="debug">0</xsl:param><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="cms">n</xsl:param><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_oid"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_oidSpecialite"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_oidUe"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_redirect"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_onglet"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_profil">amue</xsl:param><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_recherche">result</xsl:param><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="baseMediaURL">media</xsl:param><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_ecran"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="domaine"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="diplome"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="composante"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="title"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="intitule"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="titre-fr"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="intdesc"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="matiere-fr"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="basket" select="0"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_debug" select="n"/><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="documentation-uri"><xsl:choose><xsl:when test="$cms='o'">foo.xml</xsl:when><xsl:otherwise>../../../xml2xml/cdmfr-2012-rof-2.50/documentation.xml</xsl:otherwise></xsl:choose></xsl:variable><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="documentation" select="document($documentation-uri)/documentation"/><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="variablesExt-uri"><xsl:choose><xsl:when test="$cms='o'">foo.xml</xsl:when><xsl:otherwise>../../variablesExt.xml</xsl:otherwise></xsl:choose></xsl:variable><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="variablesExt" select="document($variablesExt-uri)"/><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="idP">
		<xsl:choose>
			<xsl:when test="$_oidSpecialite!=''"><xsl:value-of select="$_oidSpecialite"/></xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$_oid"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="mention"><xsl:value-of select="$codeFormation"/></xsl:variable><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="specialite"><xsl:value-of select="$_oidSpecialite"/></xsl:variable><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="ue"><xsl:value-of select="$_oidUe"/></xsl:variable><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="nidP"><xsl:value-of select="key('noeudId',$idP)/@nid"/></xsl:variable><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="langueCDM" select="/CDM/@language"/><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="ongletEnCours" select="normalize-space($_onglet)"/><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_intitule">titre-<xsl:value-of select="$_lang"/></xsl:variable><!--INCLUDE OF 'sectionCNU.cdmfr.xsl'--><xsl:template name="tSectionCNU">
		<xsl:param name="pCode"/>
		<xsl:param name="pName"/>
		<xsl:call-template name="tCommunGestionLibelle">
			<xsl:with-param name="pIdent" select="'libSectionCNU'"/>
		</xsl:call-template>
		<xsl:value-of select="@code"/><xsl:text>  </xsl:text><xsl:value-of select="@name"/>
	</xsl:template><!--FIN INCLUDE OF 'sectionCNU.cdmfr.xsl'--><!--INCLUDE OF 'contactData.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="contactData">
		<!-- ajout pour orgUnit -->
		<xsl:if test="not(contactName/text[@language=$_lang]='')">
			<xsl:apply-templates select="contactName/text[@language=$_lang]"/><br/>
		</xsl:if>
		<p>	<xsl:call-template name="tCommunGestionLibelle">
				<xsl:with-param name="pIdent" select="'libAdresse'"/>
			</xsl:call-template>
			<xsl:value-of select="adr/extadr"/><xsl:text>, </xsl:text><xsl:value-of select="adr/street"/><xsl:text>, </xsl:text><xsl:value-of select="adr/pcode"/><xsl:text> </xsl:text><xsl:value-of select="adr/locality"/><xsl:text>, </xsl:text><xsl:value-of select="adr/country"/></p>
		<p>	<xsl:call-template name="tCommunGestionLibelle">
				<xsl:with-param name="pIdent" select="'libTelephoneAbrege'"/>
			</xsl:call-template>
			<xsl:value-of select="telephone"/></p>
		<p>	<xsl:call-template name="tCommunGestionLibelle">
				<xsl:with-param name="pIdent" select="'libTelecopieAbrege'"/>
			</xsl:call-template>
			<xsl:value-of select="fax"/></p>
		<xsl:apply-templates select="email"/>
		<!--<xsl:if test="contains(email,'@')">
			<p><a href="mailto:{email}"><xsl:value-of select="email"/></a></p>
		</xsl:if>-->
		<xsl:if test="infoBlock[@blockLang=$_lang]"> <!-- Complément adresse -->
			<p><xsl:apply-templates select="infoBlock[@blockLang=$_lang]"/></p>
		</xsl:if>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="tContacts">
		<xsl:param name="pNodeContacts"/>
		<!-- section contactData -->
		<xsl:apply-templates select="$pNodeContacts/contactData"/><br/>
		<!-- section refPerson -->
		<xsl:apply-templates select="$pNodeContacts/refPerson"/><br/>
		<!-- section infoBlock -->
		<xsl:apply-templates select="$pNodeContacts/infoBlock"/><br/>
	</xsl:template><!--FIN INCLUDE OF 'contactData.cdmfr.xsl'--><!--INCLUDE OF 'level.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tLevelInfoBlockType">
		<xsl:param name="pNode"/>
		<xsl:value-of select="$pNode/level"/>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tLevelLanguageCECRLLevel">
		<xsl:param name="pNode"/>
		<xsl:value-of select="$pNode/level/@languageCECRLLevel"/>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tLevelLevel">
		<xsl:param name="pNode"/>
		<xsl:if test="$pNode/level/@level">
			<li><xsl:call-template name="tCommunGestionLibelle"><xsl:with-param name="pIdent" select="'libTypeDiplome'"/></xsl:call-template>
				<xsl:choose>
					<xsl:when test="$pNode/level/@level='L'">
						<xsl:value-of select="'Licence'"/>
					</xsl:when>
					<xsl:when test="$pNode/level/@level='M'">
						<xsl:value-of select="'Master'"/>
					</xsl:when>
					<xsl:when test="$pNode/level/@level='Doc'">
						<xsl:value-of select="'Doctorat'"/>
					</xsl:when>
					<xsl:otherwise>INCONNU</xsl:otherwise>
				</xsl:choose>
			</li>
		</xsl:if>
	</xsl:template><!--FIN INCLUDE OF 'level.cdmfr.xsl'--><!--INCLUDE OF 'learningObjectives.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tLearningObjectivesInfoBlockType">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/learningObjectives"/>
	</xsl:template><!--FIN INCLUDE OF 'learningObjectives.cdmfr.xsl'--><!--INCLUDE OF 'admissionInfo.cdmfr.xsl'--><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tAdmissionInfoStudentPlaces">
		<xsl:param name="pNode"/>
		<xsl:value-of select="$pNode/admissionInfo/studentPlaces[@blockLang=$_lang]/@places"/>
		<!--<xsl:value-of select="$pNodeAdmissionInfo/admissionInfo/studentPlaces[@blockLang=$_lang]"/>-->
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tAdmissionInfoAdmissionDescription">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/admissionInfo/admissionDescription"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tAdmissionInfoStudentStatus">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/admissionInfo/studentStatus"/>
	</xsl:template><!--FIN INCLUDE OF 'admissionInfo.cdmfr.xsl'--><!--INCLUDE OF 'recommendedPrerequisites.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tRecommendedPrerequisites">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/recommendedPrerequisites"/>
	</xsl:template><!--FIN INCLUDE OF 'recommendedPrerequisites.cdmfr.xsl'--><!--INCLUDE OF 'formalPrerequisites.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tFormalPrerequisites">
		<xsl:param name="pNode"/>
		<p><xsl:apply-templates select="$pNode/formalPrerequisites"/></p>
	</xsl:template><!--FIN INCLUDE OF 'formalPrerequisites.cdmfr.xsl'--><!--INCLUDE OF 'formOfTeaching.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tFormOfTeaching">
		<xsl:param name="pNode"/>
		<xsl:if test="count($pNode/formOfTeaching) &gt; 0">
			<ul>
				<xsl:for-each select="$pNode/formOfTeaching">
					<li>
						<xsl:apply-templates select="@method"/> : 
						<xsl:apply-templates select="node()"/>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tFormOfTeachingMethod">
		<xsl:param name="pNode"/>
		<xsl:if test="count($pNode/formOfTeaching) &gt; 0">
			<ul>
				<xsl:for-each select="$pNode/formOfTeaching">
					<li>
						<xsl:apply-templates select="@method"/>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tFormOfTeachingInfoBlockType">
		<xsl:param name="pNode"/>
		<xsl:if test="count($pNode/formOfTeaching) &gt; 0">
			<ul>
				<xsl:for-each select="$pNode/formOfTeaching">
					<li>
						<xsl:apply-templates select="node()"/>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tFormOfTeachingInfoBlockTypePDF">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/formOfTeaching"/>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tFormOfTeachingMethodPDF">
		<xsl:param name="pNode"/>
		<xsl:value-of select="$pNode/formOfTeaching/@method"/>
	</xsl:template><!--FIN INCLUDE OF 'formOfTeaching.cdmfr.xsl'--><!--INCLUDE OF 'formOfAssessment.cdmfr.xsl'--><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tFormOfAssessment">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/formOfAssessment"/>
	</xsl:template><!--FIN INCLUDE OF 'formOfAssessment.cdmfr.xsl'--><!--INCLUDE OF 'expenses.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tExpensesInfoBlockType"> 
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/expenses"/>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tExpensesPrice">
		<xsl:param name="pNode"/>
		<xsl:if test="$pNode/expenses/@price"><xsl:value-of select="$pNode/expenses/@price"/><xsl:text> </xsl:text><xsl:value-of select="$pNode/expenses/@currency"/></xsl:if>
	</xsl:template><!--FIN INCLUDE OF 'expenses.cdmfr.xsl'--><!--INCLUDE OF 'credits.cdmfr.xsl'--><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCreditsECTScredits">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/credits/@ECTScredits"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCreditsInfoBlockType">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/credits/infoBlock"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCreditsGlobalVolume">
		<xsl:param name="pNode"/>
		<ul>
			<xsl:for-each select="$pNode/credits/globalVolume">
				<li>
					<xsl:call-template name="transformTeachingtype"><xsl:with-param name="pTeachingtype" select="@teachingtype"/></xsl:call-template> :
					<xsl:apply-templates select="node()"/>
					<xsl:text> h</xsl:text>
				</li>
			</xsl:for-each>
		</ul>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCreditsGlobalVolumeTeachingType">
		<xsl:param name="pNode"/>
		<xsl:param name="pTeachingType"/><xsl:value-of select="$pNode/credits"/>
		<xsl:apply-templates select="$pNode/credits/globalVolume[@teachingtype=$pTeachingType]/node()"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="transformTeachingtype">
		<xsl:param name="pTeachingtype"/>
		<xsl:choose>
			<xsl:when test="$pTeachingtype = 'homeWork'">
				<xsl:value-of select="$variablesExt/variables/courseVar/userDefine[@type='volumeHomeWork']/value[@lang=$_lang]"/>
			</xsl:when>
			<xsl:when test="$pTeachingtype = 'ProfessTraining'">
				<xsl:value-of select="$variablesExt/variables/courseVar/userDefine[@type='volumeProfessTraining']/value[@lang=$_lang]"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$pTeachingtype"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template><!--FIN INCLUDE OF 'credits.cdmfr.xsl'--><!--INCLUDE OF 'natureElements.cdmfr.xsl'--><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUneMention">
	<!-- Cette variable locale permet de déterminer si l'on est sur une mention ou pas (pour conditionner l'affichage) -->
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureProgramID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,2)='ME'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUneSpecialite">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureProgramID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,2)='SP'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUnProgramme">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureProgramID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,2)='PR'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUnComposantProgramme">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureProgramID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,2)='CP'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUneAnnee">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureProgramID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,4)='CPAN'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUnSemestre">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureProgramID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,4)='CPSE'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUnEnseignement">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureEnseignementID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,2)='EN'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUneUE">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureEnseignementID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,4)='ENUE'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUneEC">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureEnseignementID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,4)='ENEC'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tNatureProgramID">
		<xsl:apply-templates select="key('objet',$idP)/programID"/>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tNatureEnseignementID">
		<xsl:value-of select="key('objet',$idP)/courseID"/>
	</xsl:template><!--FIN INCLUDE OF 'natureElements.cdmfr.xsl'--><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="get-var">
		<xsl:param name="xpath"/>
    <xsl:param name="location" select="'indéfinie'"/>
		<xsl:choose><xsl:when test="$xpath"><xsl:value-of select="$xpath"/></xsl:when><xsl:otherwise>INDEFINI<img src="{$baseMediaURL}/images/help.png" alt="location={$location}" title="location={$location}"/></xsl:otherwise></xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="get-lib">
		<xsl:param name="lib"/>
    <xsl:param name="location" select="'indéfinie'"/>
    
		<xsl:choose><xsl:when test="$variablesExt/variables/libelleVar/libellePart[@type=$lib]/value[@lang=$_lang]"><xsl:value-of select="$variablesExt/variables/libelleVar/libellePart[@type=$lib]/value[@lang=$_lang]"/></xsl:when><xsl:otherwise><xsl:value-of select="$lib"/><img src="{$baseMediaURL}/images/help.png" alt="location={$location}" title="location={$location}"/></xsl:otherwise></xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="getVar">
		<xsl:param name="var"/>
    <xsl:param name="location" select="'indéfinie'"/>
		<xsl:choose><xsl:when test="$variablesExt/root">CMS</xsl:when><xsl:when test="$variablesExt/variables/pageTitle/header[@type=$var]/value[@lang=$_lang]"><xsl:value-of select="$variablesExt/variables/pageTitle/header[@type=$var]/value[@lang=$_lang]"/></xsl:when><xsl:otherwise><xsl:value-of select="$var"/><img src="{$baseMediaURL}/images/help.png" alt="location={$location}" title="location={$location}"/></xsl:otherwise></xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="get-nvar">
		<xsl:param name="nvar"/>
    <xsl:param name="location" select="'indéfinie'"/>
		<xsl:choose><xsl:when test="$variablesExt/variables/navigationVar/link[@type=$nvar]/navigationItem/value[@lang=$_lang]"><xsl:value-of select="$variablesExt/variables/navigationVar/link[@type=$nvar]/navigationItem/value[@lang=$_lang]"/></xsl:when><xsl:otherwise><xsl:value-of select="$nvar"/><img src="{$baseMediaURL}/images/help.png" alt="location={$location}" title="location={$location}"/></xsl:otherwise></xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="get-rub">
		<xsl:param name="rub"/>
    <xsl:param name="location" select="'indéfinie'"/>
		<xsl:choose><xsl:when test="$variablesExt/variables/titreVar/titreRub[@type=$rub]/value[@lang=$_lang]"><xsl:value-of select="$variablesExt/variables/titreVar/titreRub[@type=$rub]/value[@lang=$_lang]"/></xsl:when><xsl:otherwise><xsl:value-of select="$rub"/><img src="{$baseMediaURL}/images/help.png" alt="location={$location}" title="location={$location}"/></xsl:otherwise></xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tUser-col_droite">
		<xsl:param name="pNom"/>
		<xsl:param name="pContactData"/>
		<xsl:param name="pLienLibelle"/>
		<xsl:param name="pLienURL"/>
		<xsl:if test="not($pNom ='')">
			<h3><xsl:value-of select="$pNom"/></h3>
		</xsl:if>
		<xsl:if test="not($pContactData ='')">
			<xsl:apply-templates select="/CDM/orgUnit[1]/contacts/contactData"/>
		</xsl:if>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="webLink" mode="enSavoirPlus">
		<xsl:choose>
			<xsl:when test="not(normalize-space(href))='' and not(normalize-space(linkName))=''"><p><a href="{href}"><xsl:value-of select="linkName"/></a></p></xsl:when>
			<xsl:when test="not(normalize-space(href))=''"><p><a href="{href}"><xsl:value-of select="href"/></a></p></xsl:when>
		</xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tLienEnSavoirPlus">
		<xsl:param name="pLienURL"/>
		<xsl:if test="not(normalize-space($pLienURL) ='')">
			<a href="{$pLienURL}"><xsl:value-of select="$pLienURL"/></a>
		</xsl:if>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="searchword[position()=1]">
		<xsl:value-of select="text()"/>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="searchword[position()&gt;1]"><xsl:text>, </xsl:text><xsl:value-of select="text()"/>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="lov[@info='erasmus']">
		<li>[<xsl:value-of select="@code"/>] <xsl:value-of select="text()"/></li>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="lov[@info='rome']">
		<li>[<xsl:value-of select="@code"/>] <xsl:value-of select="text()"/></li>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCommunGestionLibelle">
		<xsl:param name="pIdent"/>
		<xsl:param name="idDoc" select="'vide'"/>
		<xsl:call-template name="tCommunDoc"><xsl:with-param name="idDoc" select="$idDoc"/></xsl:call-template>
    <xsl:call-template name="get-lib">
      <xsl:with-param name="lib"><xsl:value-of select="normalize-space($pIdent)"/></xsl:with-param>
    </xsl:call-template>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCommunDoc">
		<xsl:param name="idDoc" select="'vide'"/>
		<xsl:param name="element" select="$documentation/element[@id=$idDoc]"/>
		<xsl:if test="$element/@id and $_debug='y'">
			<a class="documentation" href="">?</a>
			<div class="documentation" style="display:none">
				<ul>
					<xsl:choose>
						<xsl:when test="$element/@cdmObjet='uniform'"><xsl:apply-templates select="$element"><xsl:with-param name="objet" select="/CDM/properties/infoBlock/extension/uniform"/></xsl:apply-templates></xsl:when>
						<xsl:when test="$element/@cdmObjet='properties'"><xsl:apply-templates select="$element"><xsl:with-param name="objet" select="/CDM/properties"/></xsl:apply-templates></xsl:when>
						<xsl:when test="$element/@cdmObjet='habilitation'"><xsl:apply-templates select="$element"><xsl:with-param name="objet" select="key('objet',$idP)/habilitation"/></xsl:apply-templates></xsl:when>
						<xsl:otherwise><xsl:apply-templates select="$element"><xsl:with-param name="objet" select="key('objet',$idP)"/></xsl:apply-templates></xsl:otherwise>
					</xsl:choose>
				</ul>
			</div>
		</xsl:if>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="*" mode="transform">&lt;<xsl:value-of select="name()"/> <xsl:apply-templates select="@*" mode="transform"/><xsl:if test="not(*|text())">/</xsl:if>&gt;<xsl:if test="*|text()"><xsl:apply-templates select="*|text()" mode="transform"/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:if></xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="@*" mode="transform"><xsl:text> </xsl:text><xsl:value-of select="name()"/>="<xsl:value-of select="."/>"</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="text()" mode="transform"><xsl:value-of select="."/></xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="*|@*" mode="debug">
		<xsl:param name="path"/>
		<xsl:choose>
			<xsl:when test="local-name()=$path or $path=''"><xsl:apply-templates select="." mode="transform"/></xsl:when>
			<xsl:otherwise><xsl:apply-templates select="*|@*" mode="debug"><xsl:with-param name="path" select="$path"/></xsl:apply-templates></xsl:otherwise>
		</xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="element">
		<xsl:param name="objet"/>
		<xsl:variable name="xpath">
			<xsl:if test="@xpath">
				<pre>
					<xsl:apply-templates select="$objet" mode="debug">
						<xsl:with-param name="path" select="@xpath"/>
					</xsl:apply-templates>
				</pre>
			</xsl:if>
		</xsl:variable>
		<li>
			<p><xsl:value-of select="@label"/> 
				<xsl:choose><xsl:when test="@href and @value"> : <a href="{@href}"><xsl:value-of select="@value"/></a></xsl:when><xsl:when test="@value"> : <xsl:value-of select="@value"/></xsl:when><xsl:otherwise/></xsl:choose>
				<xsl:text> </xsl:text><xsl:if test="@cdmObjet">Objet : "<xsl:value-of select="@cdmObjet"/>"</xsl:if>
			</p>
			<xsl:value-of select="$idP"/>
			<xsl:if test="element">
				<ul>
					<xsl:apply-templates select="element"/>
				</ul>
			</xsl:if>
			<xsl:apply-templates select="p"/>
			<xsl:copy-of select="$xpath"/>
		</li>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="p">
		<xsl:copy-of select="."/>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="adr">
		<xsl:if test="string-length(pobox)+string-length(extadr)+string-length(street)+string-length(pcode)+string-length(locality)&gt;0">
			<p>
				<xsl:if test="string-length(pobox)&gt;0"><xsl:value-of select="pobox"/>,<xsl:text> </xsl:text></xsl:if>
				<xsl:if test="string-length(extadr)&gt;0"><xsl:value-of select="extadr"/>,<xsl:text> </xsl:text></xsl:if> 
				<xsl:if test="string-length(street)&gt;0"><xsl:value-of select="street"/></xsl:if><xsl:if test="string-length(pcode)+string-length(locality)&gt;0"><xsl:text>, 
				</xsl:text><xsl:value-of select="pcode"/><xsl:text> </xsl:text><xsl:value-of select="locality"/></xsl:if>
			</p>
		</xsl:if>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="ariane2">
		<xsl:apply-templates select="/CDM/properties[@facet=$facet]/infoBlock/extension/hierarchy/noeud" mode="ariane2"/>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="noeud" mode="ariane2">
		&gt; <xsl:call-template name="construireLien"><xsl:with-param name="noeud" select="."/></xsl:call-template>
		<xsl:if test="@nid!=$nidP"><xsl:apply-templates select="noeud[descendant-or-self::noeud/@nid=$nidP][1]" mode="ariane2"/></xsl:if>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="construireLien">
		<xsl:param name="noeud"/>
		<xsl:variable name="ecran">fiche</xsl:variable>
		<xsl:variable name="onglet">description</xsl:variable>
		
		<xsl:choose>
			<xsl:when test="$nidP!=$noeud/@nid">
				<xsl:choose>
					<xsl:when test="$noeud/@typeObjet!='course'">
						<!-- cas gÃ©nÃ©ral : Parcours, annÃ©e, semestres -->
						<a href="{$absActionURL}/{$_lang}/{$ecran}/programme/{$mention}/{$noeud/@id}">
							<xsl:text> </xsl:text><xsl:value-of select="$noeud/@libelle"/><xsl:text> </xsl:text>
						</a>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$absActionURL}/{$_lang}/{$ecran}/programme/{$mention}/{$noeud/@id}#ficheEC">
							<xsl:text> </xsl:text><xsl:value-of select="$noeud/@libelle"/><xsl:text> </xsl:text>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text><xsl:value-of select="$noeud/@libelle"/><xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="listeElementsDossier">
		<ul type="none">
			<xsl:apply-templates select="/CDM/properties[@facet=$facet]/infoBlock/extension/hierarchy/noeud" mode="listeElementsDossier"/>
		</ul>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="noeud" mode="listeElementsDossier">
		<!-- Cas ou tout est depliÃ© -->
		<xsl:choose>
			<xsl:when test="$_oidUe = 'deplier'">
				<li>
					<xsl:call-template name="construireLien"><xsl:with-param name="noeud" select="."/></xsl:call-template>
					<xsl:if test="noeud">
						<ul>
							<xsl:apply-templates select="noeud" mode="listeElementsDossier"/>
						</ul>	
					</xsl:if>
				</li>
			</xsl:when>
			<xsl:when test="$_oidUe = 'replier'">
				<li style="list-style-type:none">
					<xsl:if test="noeud[position()!=last()]">
						<a href="javascript:void(0);" onclick="show_hide('bloc_{@nid}','img_{@nid}', '{$baseMediaURL}');">
							<img id="img_{@nid}" src="{$baseMediaURL}/images/deplier.jpg" border="0"/>
						</a>
					</xsl:if>
					<xsl:call-template name="construireLien"><xsl:with-param name="noeud" select="."/></xsl:call-template>
					<xsl:if test="noeud">
						<ul>
							<xsl:choose>
								<xsl:when test="noeud[position() &gt; 1]">
									<div id="bloc_{@nid}" style="display:none" class="pliage">
										<xsl:apply-templates select="noeud" mode="listeElementsDossier"/>
									</div>
								</xsl:when>
								<xsl:otherwise>
									<div id="bloc_{@nid}" class="pliage">
										<xsl:apply-templates select="noeud" mode="listeElementsDossier"/>
									</div>
								</xsl:otherwise>
							</xsl:choose>
						</ul>
					</xsl:if>
				</li>
			</xsl:when>
			<xsl:otherwise>
				<li style="list-style-type:none">
					<xsl:choose>
						<!-- 1er cas : on est dans l'arborescence : affichage des noeuds parents -->
						<xsl:when test="noeud[descendant-or-self::noeud/@nid=$nidP][1]">
							<xsl:if test="noeud[position()!=last()]">
								<a href="javascript:void(0);" onclick="show_hide('bloc_{@nid}','img_{@nid}', '{$baseMediaURL}');">
									<img id="img_{@nid}" src="{$baseMediaURL}/images/plier.jpg" border="0"/>
								</a>
							</xsl:if>
							<xsl:call-template name="construireLien"><xsl:with-param name="noeud" select="."/></xsl:call-template>
								<ul>
									<div id="bloc_{@nid}" class="pliage"><xsl:apply-templates select="noeud" mode="listeElementsDossier"/>
									</div>
								</ul>
						</xsl:when>
						<!-- Sinon, on est au niveau de la mention -->
						<xsl:otherwise>
							<xsl:if test="noeud[position()!=last()]">
								<a href="javascript:void(0);" onclick="show_hide('bloc_{@nid}','img_{@nid}', '{$baseMediaURL}');">
									<img id="img_{@nid}" src="{$baseMediaURL}/images/deplier.jpg" border="0"/>
								</a>
							</xsl:if>
							<xsl:call-template name="construireLien"><xsl:with-param name="noeud" select="."/></xsl:call-template>
							<xsl:if test="noeud">
								<ul>
									<xsl:choose>
										<xsl:when test="noeud[position() &gt; 1]">
											<div id="bloc_{@nid}" style="display:none" class="pliage">
												<xsl:apply-templates select="noeud" mode="listeElementsDossier"/>
											</div>
										</xsl:when>
										<xsl:otherwise>
											<div id="bloc_{@nid}" class="pliage">
												<xsl:apply-templates select="noeud" mode="listeElementsDossier"/>
											</div>
										</xsl:otherwise>
									</xsl:choose>
								</ul>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</li>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template><!--FIN INCLUDE OF '../commun/composants/commun/commun.xsl'-->
	<!--INCLUDE OF '../lille1-1.0/xhtml.xsl'--><xsl:param xmlns="http://www.w3.org/1999/xhtml" name="url">http://formations.univ-lille1.fr</xsl:param><xsl:param xmlns="http://www.w3.org/1999/xhtml" name="titreCatalogue">Catalogue des formations de l'université Lille 1 - Sciences et technologies</xsl:param><!--INCLUDE OF 'user-xhtml.xsl'--><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="ariane">
		<xsl:choose><xsl:when test="$idP=$_oid"><xsl:value-of select="key('objet', $_oid)/programName/text[@language=$_lang]"/></xsl:when><xsl:otherwise><a href="{$absActionURL}/{$_lang}/fiche/description/{$_oid}"><xsl:value-of select="key('objet', $_oid)/programName/text[@language=$_lang]"/></a> &gt; <xsl:value-of select="substring-after(key('objet', $idP)/programName/text[@language=$_lang],' ')"/></xsl:otherwise></xsl:choose>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-titreCatalogue">

  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-recherche">
    <section id="recherche" class="clearfix">
					<h3 class="title-lille1">Recherche</h3>
					<form role="form" action="{$absActionURL}/{$_lang}/liste" method="post">
          				<input type="hidden" name="_debut" value="0"/>        
                  <input type="hidden" name="_pas" value="200"/>
                  <input type="hidden" name="__1" value="__domaine"/>
                  <input type="hidden" name="__2" value="AND"/>
                  <input type="hidden" name="__3" value="__diplome"/>
                  <input type="hidden" name="__4" value="AND"/>
                  <input type="hidden" name="__5" value="__composante"/>
                  <input type="hidden" name="__6" value="AND"/>
                  <input type="hidden" name="__7" value="__matiere-fr_pt_AND"/>
                  <input type="hidden" name="__8" value="AND"/>
                  <input type="hidden" name="__9" value="__titre-fr_pt_AND"/>
                  <input type="hidden" name="_rechercheActive" value="o"/>
						
            <div class="form-group">
							<label for="search-diplome" class="sr-only">Diplôme</label>
							<select class="form-control input-sm" id="search-diplome" size="1" name="__diplome">
								<option value=""><xsl:if test="$diplome=''"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>par diplôme...</option>
                <xsl:for-each select="/CDM/properties/infoBlock/extension/uniform/constantes/typeDiplome/data">
                  <option value="{./@id}">
                    <xsl:if test="./@id=$diplome"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
                    <xsl:value-of select="./value[@lang=$_lang]"/>
                  </option>
                </xsl:for-each>
							</select>
						</div>
            <div class="form-group">
							<label for="search-domaine" class="sr-only">Domaine</label>
							<select class="form-control input-sm" id="search-domaine" size="1" name="__domaine">
								<option value=""><xsl:if test="$domaine=''"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>Par domaine...</option>
                <xsl:for-each select="/CDM/properties/infoBlock/extension/uniform/constantes/domaineDiplome[@type='domaine']/data">
                  <option value="{./@id}">
                    <xsl:if test="./@id=$domaine"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
                    <xsl:value-of select="./value[@lang=$_lang]"/>
                  </option>
                </xsl:for-each>
							</select>
						</div>
						<div class="form-group">
							<label for="search-composante" class="sr-only">Composante</label>
							<select class="form-control input-sm" id="search-composante" size="1" name="__composante">
                <option value=""><xsl:if test="$composante=''"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>Toutes les composantes</option>		
                <xsl:for-each select="/CDM/orgUnit/orgUnit">
                  <xsl:sort select="orgUnitName/text"/>
                  <option value="{./@ident}">
                    <xsl:if test="./@ident=$composante"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
                    <xsl:value-of select="orgUnitName/text"/>
                  </option>
                </xsl:for-each>
							</select>
						</div>
						<div class="form-group">
							<label for="search-matieres" class="sr-only">Matières enseignées</label>
							<select class="form-control input-sm" id="search-matieres" size="1" name="__matiere-fr_pt_AND">
								<option value=""><xsl:if test="$matiere-fr=''"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>Toutes les matières</option>
                <xsl:for-each select="/CDM/properties/infoBlock/extension/uniform/constantes/matiere/data">
                  <option value="{./value[@lang=$_lang]}">
                    <xsl:if test="./value[@lang=$_lang]=$matiere-fr"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
                    <xsl:value-of select="./value[@lang=$_lang]"/>
                  </option>
                </xsl:for-each>
							</select>
						</div>
						<div class="form-group">
							<label for="search-keywords" class="sr-only">Mots-clés</label>
              <input id="search-keywords" name="__titre-{$_lang}_pt_AND" type="text" class="form-control" placeholder="mots-clés...">
                <xsl:attribute name="value"><xsl:value-of select="$titre-fr"/></xsl:attribute>
              </input>
						</div>
						<button type="submit" class="btn btn-default pull-right"><span class="glyphicon glyphicon-search"><xsl:text> </xsl:text></span> Rechercher</button>
					</form>
				</section>
 	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" match="webLink" mode="accueil">
    <li><a href="{href}"><xsl:value-of select="linkName"/></a></li>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-liens">
    <section id="links">
      <h3 class="title-lille1">Liens utiles</h3>
      <ul class="list-unstyled">
        <xsl:apply-templates select="/CDM/orgUnit[1]/webLink[@role='link']" mode="accueil"/>
      </ul>
    </section>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-devenir_ens">
    <!--section id="devenir_ens">
      <h3 class="title-lille1">Devenir enseignant</h3>
      <ul class="list-unstyled">
        <li><a href="http://www.univ-lille1.fr/etudes/Organisation-etudes/Preparations_concours_enseignement">Devenir enseignant : ce qui change à la rentrée 2013 !</a></li>
        <li><a href="http://www-espe-lille-norddefrance.lille.iufm.fr/">ESPE Lille Nord de France</a></li>
      </ul>
    </section-->
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-contact">
    <section id="contact">
      <h3 class="title-lille1">Contact : </h3>
      <ul class="list-unstyled">
        <li><a href="{/CDM/orgUnit[1]/webLink[@role='mail'][1]/href}"><xsl:value-of select="/CDM/orgUnit[1]/webLink[@role='mail'][1]/linkName"/></a></li>
      </ul>
    </section>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-langue">

	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" match="uniform/constantes/facet/language/data">
		<span><a href="{$absActionURL}/{@id}/accueil"><xsl:value-of select="value[@lang=$_lang]"/></a> </span>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-pied">
		<footer class="center-page">
      <hr class="visible-print"/>
      <p class="visible-print">Université Lille1 Sciences et Technologies</p>
      <p>Cité Scientifique 59655 Villeneuve d'Ascq Cedex  Tél. +33 (0) 3.20.43.43.43 <br/>
        <a class="screen" href="{$absActionURL}/{$_lang}/accueil/mentionsLegales" accesskey="8">Mentions légales</a>
      </p>
      
    </footer>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-css">
		<!-- custom Lille1 -->
    <link href="{$absActionURL}/media/css/main.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{$absActionURL}/media/css/print.css" rel="stylesheet" type="text/css" media="print"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-scripts">
	  <!-- ordre de chargement IMPORTANT -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"><xsl:text>//
    </xsl:text></script>   
    <script src="{$absActionURL}/media/bootstrap/js/bootstrap.min.js" type="text/javascript"><xsl:text>//
    </xsl:text></script>   
    
    <xsl:call-template name="page-script"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" match="linkItem">
    <option value="{itemValue}"><xsl:value-of select="itemName"/></option>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" match="groupItems">
    <optgroup label="{@value}">
      <xsl:apply-templates/>
    </optgroup>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="plan-site">
		<ul>
			<li><a href="{$absActionURL}/{$_lang}/accueil">Accueil</a></li>
			<li><a href="{$absActionURL}/{$_lang}/liste">Feuilleter le catalogue</a></li>
			<li><a href="{$absActionURL}/{$_lang}/recherche">Recherche avancée</a></li>
			<li><a href="{$absActionURL}/{$_lang}/accueil/accessibilite">Accessibilité</a></li>
			<li><a href="{$absActionURL}/{$_lang}/accueil">Mentions légales</a></li>
			<li><a href="{$absActionURL}/{$_lang}/plan-site">Plan du Site</a></li>
		</ul>
	</xsl:template><!--FIN INCLUDE OF 'user-xhtml.xsl'--><!--INCLUDE OF 'user-contacts.xsl'--><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="contact-suaio">
	<div class="subinfo">
		<h4>Orientation - Information</h4>
		<span class="image"><img src="{$absActionURL}/media/images/mel.png" alt="Email : "/><a href="mailto:suaio@univ-lille1.fr" title="Lien vers le mail de suaio@univ-lille1.fr">suaio@univ-lille1.fr</a></span>
		<span class="image"><img src="{$absActionURL}/media/images/tel.png" alt="Tel : "/>+33 (0) 320 05 87 49</span>
		<span class="image"><img src="{$absActionURL}/media/images/link.png" alt="Lien : "/><a href="http://suaio.univ-lille1.fr/" onclick="return nleFenetre('http://suaio.univ-lille1.fr/')" title="Lien vers site web SUAIO">site web SUAIO</a></span>
	</div>
</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="contact-admission">
	<div class="subinfo">
		<h4>Admission - Inscription</h4>
		<span class="image"><img src="{$absActionURL}/media/images/link.png" alt="Lien : "/><a href="http://www.univ-lille1.fr/etudes/Admission-Inscription" onclick="return nleFenetre('http://www.univ-lille1.fr/etudes/Admission-Inscription')" title="Lien vers site web Admission">site web Admission</a></span>
	</div>
</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="contact-vie">
	<div class="subinfo">
		<h4>Vie étudiante</h4>
		<span class="image"><img src="{$absActionURL}/media/images/mel.png" alt="Email : "/><a href="http://www.univ-lille1.fr/campus/" onclick="return nleFenetre('http://www.univ-lille1.fr/campus/')" title="Lien vers Site web Vie étudiante">Site web Vie étudiante</a></span>
	</div>
</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="contact-poursuites">
	<div class="subinfo">
		<h4>Poursuites d'études - Insertion professionnelle</h4>
		<span class="image"><img src="{$absActionURL}/media/images/mel.png" alt="Email : "/><a href="mailto:pass-pro@univ-lille1.fr" title="Lien vers le mail de pass-pro@univ-lille1.fr">pass-pro@univ-lille1.fr</a></span>
		<span class="image"><img src="{$absActionURL}/media/images/link.png" alt="Lien : "/><a href="http://pass-pro.univ-lille1.fr/" onclick="return nleFenetre('http://pass-pro.univ-lille1.fr/')" title="Lien vers Pass'Pro : Bureau d'Aide à l'Insertion Professionnelle">Pass'Pro : Bureau d'Aide à l'Insertion Professionnelle</a></span>
		<br/><br/>
		<span class="image"><img src="{$absActionURL}/media/images/mel.png" alt="Email : "/><a href="mailto:ofip@univ-lille1.fr" title="Lien vers le mail de ofip@univ-lille1.fr">ofip@univ-lille1.fr</a></span>
		<span class="image"><img src="{$absActionURL}/media/images/link.png" alt="Lien : "/><a href="http://ofip.univ-lille1.fr/" onclick="return nleFenetre('http://ofip.univ-lille1.fr/')" title="Lien vers OFIP : Observatoire des Formations et de l'Insertion Professionnelle">Observatoire des Formations et de l'Insertion Professionnelle</a></span>
		<br/><br/>
		<span class="image"><img src="{$absActionURL}/media/images/mel.png" alt="Email : "/><a href="mailto:suaio@univ-lille1.fr" title="Lien vers le mail de suaio@univ-lille1.fr">suaio@univ-lille1.fr</a></span>
		<span class="image"><img src="{$absActionURL}/media/images/link.png" alt="Lien : "/><a href="http://suaio.univ-lille1.fr/" onclick="return nleFenetre('http://suaio.univ-lille1.fr/')" title="Lien vers SUAIO : Service Universitaire d'Accueil Information Orientation">Service Universitaire d'Accueil Information Orientation</a></span>
	</div>
</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="contact-ri">
	<div class="subinfo">
		<h4>Relations internationales</h4>
		<span class="image"><img src="{$absActionURL}/media/images/link.png" alt="Lien : "/><a href="http://ci.univ-lille1.fr" onclick="return nleFenetre('http://ci.univ-lille1.fr')" title="Lien vers site web Centre International">Site web Centre International</a></span>
	</div>
</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="refPerson" mode="role">
		<xsl:apply-templates select="key('objet',@ref)" mode="role">
			<xsl:with-param name="role" select="@role"/>
		</xsl:apply-templates>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="person" mode="role">
		<xsl:param name="role"/>
		<xsl:variable name="leRole">
			<xsl:if test="$role!=''">
				<xsl:value-of select="normalize-space($role)"/>
			</xsl:if>
			<xsl:if test="$role=''">
				<xsl:value-of select="normalize-space(./role)"/>
			</xsl:if>
		</xsl:variable>
		<div class="subinfo">
			<h4><xsl:value-of select="$leRole"/></h4>
			<xsl:apply-templates select="./contactData" mode="contact"/>
		</div>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="extadr" mode="contact">
		<xsl:if test="not(normalize-space(.)='')">
			<span class="image"><img src="{$absActionURL}/media/images/adr.png" alt="Adresse : "/>
				<xsl:value-of select="."/>
			</span>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="contactData" mode="contact">
		<!-- Traitement mail vide -->
		<xsl:variable name="nom">
			<xsl:choose>
				<xsl:when test="not(normalize-space(../name/family)='')">
					<xsl:value-of select="../name/family"/>
				</xsl:when>
				<xsl:when test="email">
					<xsl:value-of select="./email"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="not(./email='')">
				<span class="image"><img src="{$absActionURL}/media/images/mel.png" alt="Email : "/><a href="mailto:{email}" title="adresse électronique"><xsl:value-of select="$nom"/></a></span>
			</xsl:when>
			<xsl:otherwise>
				<span><xsl:value-of select="$nom"/></span>
			</xsl:otherwise>
		</xsl:choose>
		<!-- Traitement telephone et Fax vide -->
		<xsl:if test="not(normalize-space(./telephone)='')">
			<span class="image"><img src="{$absActionURL}/media/images/tel.png" alt="Tel : "/>
				<xsl:value-of select="./telephone"/>
			</span>
		</xsl:if>
		<xsl:if test="not(normalize-space(./fax)='')">
			<span class="image"><img src="{$absActionURL}/media/images/fax.png" alt="Fax : "/>
				<xsl:value-of select="./fax"/>
			</span>
		</xsl:if>
		<xsl:if test="not(normalize-space(./webLink)='')">
			<xsl:apply-templates select="./webLink">
				<xsl:with-param name="lienExterieur" select="1"/>
				<xsl:with-param name="menu" select="1"/>
			</xsl:apply-templates>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="plan-recherche"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="user-link"/><!--FIN INCLUDE OF 'user-contacts.xsl'--><!--INCLUDE OF 'user-commun.xsl'--><xsl:key xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="program" match="/CDM/properties/infoBlock/extension/uniform/constantes/catalogue//program" use="@id"/><!--FIN INCLUDE OF 'user-commun.xsl'--><xsl:param xmlns="http://www.w3.org/1999/xhtml" name="urlSites"><xsl:choose><xsl:when test="$cms='o'">foo.xml</xsl:when><xsl:otherwise>http://www.univ-lille1.fr/composants/acces_directs_xml</xsl:otherwise></xsl:choose></xsl:param><xsl:variable xmlns="http://www.w3.org/1999/xhtml" name="redirections" select="document($urlSites)/selectItems"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="html">
    <xsl:choose>
      <xsl:when test="$cms='o'"><xsl:call-template name="html-cms"/></xsl:when>
      <xsl:otherwise>
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
        <html lang="fr">
          <head>
            <xsl:call-template name="html-header"/>
          </head>
          <body>
            <header>
              <xsl:call-template name="centre-accessibilite"/>
              <xsl:call-template name="acces"/>
            </header>
            <xsl:call-template name="html-body"/>
            <xsl:call-template name="user-pied"/>
            <xsl:call-template name="user-scripts"/>
            <xsl:call-template name="user-stat"/>
          </body>
        </html>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="html-cms">
				<xsl:call-template name="html-body-cms"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="html-header">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    	<!-- responsive IE/iOS -->
    	<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    	<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>
			<xsl:call-template name="user-titreFenetre"/>
		</title>
    <link rel="icon" href="{$baseMediaURL}/images/favicon.ico"/>
    <!-- Bootstrap -->
    <link href="{$baseMediaURL}/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<xsl:call-template name="user-css"/>
    <script>
      function changeMenu() {
        var selectBox = document.getElementById("panels-selector");
        var selectedValue = selectBox.options[selectBox.selectedIndex].value;
        $('#tabs_menu a[href="'+selectedValue+'"]').tab('show'); // Select tab by name
      }
</script>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="html-body">
    <div id="container" class="center-page">
      <article>
          <header id="page_header">
            <a href="index.html"><img src="{$baseMediaURL}images/logo.jpg" alt="logo Université Lille 1"/></a> 
            <h1>Catalogue des Formations</h1>
          </header>
        <xsl:call-template name="user-ariane"/>    
        <div class="row">
          <xsl:call-template name="user-col_gauche"/>
          <xsl:call-template name="user-col_droite"/>
        </div>
      </article>
    </div>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="html-body-cms">
        
        <div class="row">
          <xsl:call-template name="user-col_gauche"/>
          <xsl:call-template name="user-col_droite"/>
        </div>
        <xsl:call-template name="user-stat-cms"/>

  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="centre-accessibilite">
        <div id="centre-accessibilite" class="sr-only">
      <div id="zone-accessibilite">
        <ul>
          <li><a href="#tabs_menu">Aller au menu</a> |<xsl:text> </xsl:text></li>
        	<li><a href="#content" accesskey="s">Aller au contenu</a> |<xsl:text> </xsl:text>;</li>
        	<li><a href="#recherche">Aller à la recherche</a> |<xsl:text> </xsl:text></li>
          <li><a href="http://www.univ-lille1.fr/Accueil/Accessibilite" accesskey="0">Accessibilité</a></li>
        </ul>
      </div>
    </div>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="acces">
    <nav id="acces">
      <form action="http://www.univ-lille1.fr/Accueil/redirection" method="get" class="hidden-xs">
        <div>
          <div id="englobe_acces">
            <p>
              <label for="acces_direct">Accéder aux autres sites de l'Université Lille 1:</label>
            </p>
            <select name="lien" id="acces_direct">
              <xsl:apply-templates select="$redirections"/>
            </select>
            <input type="submit" title="Accéder au site" value="ok"/>
          </div>
        </div>
      </form>
      <!-- ce lien apparait quand < 767px de large pour remplacer le SELECT --> 
      <a href="http://www.univ-lille1.fr" title="Université lille 1" class="btn btn-default btn-xs pull-right visible-xs" id="goto_lille1"><span class="glyphicon glyphicon-globe"><xsl:text> </xsl:text></span><xsl:text> </xsl:text>Université Lille 1</a> 
    </nav>
  </xsl:template><!--FIN INCLUDE OF '../lille1-1.0/xhtml.xsl'-->
	<!--INCLUDE OF '../commun/composants/orgUnit.cdmfr.xsl'--><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="refOrgUnit">
		<li><xsl:call-template name="tCommunGestionLibelle">
				<xsl:with-param name="pIdent" select="'libEtablissementDeReference'"/>
			</xsl:call-template><xsl:call-template name="tOrgUnitName"/></li>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="refOrgUnit[not(@ref)]"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitName">
		<xsl:value-of select="/CDM/orgUnit[1]/orgUnitName"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitSecondName">
		<xsl:param name="pSecondNameNature" select="'patronyme'"/>
		<xsl:value-of select="/CDM/orgUnit[1]/orgUnitSecondName[@secondNameNature=$pSecondNameNature]/secondName/text"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitCode">
		<xsl:param name="pCodeSet" select="'codeUAI'"/>
		<xsl:value-of select="/CDM/orgUnit[1]/orgUnitCode[@codeSet=$pCodeSet]"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitKind">
		<xsl:apply-templates select="/CDM/orgUnit[1]/orgUnitKind"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitWebLinkHref">
		<xsl:apply-templates select="/CDM/orgUnit[1]/webLink/href"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitDescription">
		<xsl:apply-templates select="/CDM/orgUnit[1]/orgUnitDescription"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitIntroduction">
		<xsl:apply-templates select="/CDM/orgUnit[1]/orgUnitIntroduction"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitAdmissionInfo">
		<xsl:call-template name="tOrgUnitAdmissionInfoAdmissionDescription"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitAdmissionInfoAdmissionDescription">
		<xsl:apply-templates select="/CDM/orgUnit[1]/admissionInfo/admissionDescription"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitRegistrationDeadline">
		<xsl:param name="pLibelleDateDeadline"/>
		<xsl:if test="not($pLibelleDateDeadline='')">
			<xsl:value-of select="$pLibelleDateDeadline"/> <xsl:value-of select="/CDM/orgUnit[1]/admissionInfo/registrationDeadline/@date"/>
		</xsl:if>
		<xsl:apply-templates select="/CDM/orgUnit[1]/admissionInfo/registrationDeadline"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitCancelDeadline">
		<xsl:param name="pLibelleDateCancel"/>
		<xsl:if test="not($pLibelleDateCancel='')">
			<xsl:value-of select="$pLibelleDateCancel"/> <xsl:value-of select="/CDM/orgUnit[1]/admissionInfo/cancelDeadline/@date"/>
		</xsl:if>
		<xsl:apply-templates select="/CDM/orgUnit[1]/admissionInfo/cancelDeadline"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitRegulations">
		<xsl:apply-templates select="/CDM/orgUnit[1]/regulations[@blockLang=$_lang or not(@blockLang)]"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitExpensesInfoBlockType">
		<xsl:call-template name="tExpensesInfoBlockType">
			<xsl:with-param name="pNode" select="/CDM/orgUnit[1]"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitStudentFacilities">
		<xsl:apply-templates select="/CDM/orgUnit[1]/studentFacilities[@blockLang=$_lang or not(@blockLang)]"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitUniversalAdjustment">
		<xsl:apply-templates select="/CDM/orgUnit[1]/universalAdjustment[@blockLang=$_lang or not(@blockLang)]"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitQualiteSignataireDiplome">
		<xsl:apply-templates select="/CDM/orgUnit[1]/infoBlock"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitContactData">
		<xsl:apply-templates select="/CDM/orgUnit[1]/contacts/contactData"/>
	</xsl:template><!--FIN INCLUDE OF '../commun/composants/orgUnit.cdmfr.xsl'-->
	<!--INCLUDE OF '../commun/composants/properties.cdmfr.xsl'--><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="pNode" select="/CDM/properties"/><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tPropertiesDateTimeDate">
		<xsl:value-of select="$pNode/datetime/@date"/>		
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tPropertiesDateTimeTransformDate">
		<xsl:value-of select="substring($pNode/datetime/@date,9,2)"/>
		<xsl:text>-</xsl:text>
		<xsl:value-of select="substring($pNode/datetime/@date,6,2)"/>
		<xsl:text>-</xsl:text>
		<xsl:value-of select="substring($pNode/datetime/@date,1,4)"/>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tPropertiesDateTimeTime">
		<xsl:value-of select="$pNode/datetime/@time"/>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tPropertiesDateTimeTransformTime">
		<xsl:value-of select="substring($pNode/datetime/@time,1,2)"/>
		<xsl:text>H</xsl:text>
		<xsl:value-of select="substring($pNode/datetime/@time,4,2)"/>
	</xsl:template><!--FIN INCLUDE OF '../commun/composants/properties.cdmfr.xsl'-->
	<!--INCLUDE OF '../commun/composants/schemaEtudes.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tSchemaEtudes">
	
	<img id="schemaEtudes" src="{$baseMediaURL}/images/schema.gif" alt="Schema du LMD" height="423" width="440" usemap="#schemabfaced4a" border="1"/>
		<map name="schemabfaced4a" id="schemabfaced4a">
			<area title="Licences Généralistes" shape="poly" coords="219,281,355,281,355,401,64,402,65,341,164,341,165,321,217,321" alt="Lien vers les Licences Généralistes">
			  <xsl:attribute name="href"><xsl:value-of select="$absActionURL"/>/<xsl:value-of select="$_lang"/>/liste/licence</xsl:attribute>
			</area>
			<area title="DUT" shape="rect" coords="4,322,27,402" alt="Lien vers les DUT">
			  <xsl:attribute name="href"><xsl:value-of select="$absActionURL"/>/<xsl:value-of select="$_lang"/>/liste/DUT</xsl:attribute>
			</area>
			<area title="DEUST" shape="rect" coords="35,322,57,380" alt="Lien vers les DEUST">
			  <xsl:attribute name="href"><xsl:value-of select="$absActionURL"/>/<xsl:value-of select="$_lang"/>/liste/DEUST</xsl:attribute>
			</area>
			<area title="Ingénieur" shape="rect" coords="164,201,187,320" alt="Lien vers les Ingénieur">
			  <xsl:attribute name="href"><xsl:value-of select="$absActionURL"/>/<xsl:value-of select="$_lang"/>/liste/ING</xsl:attribute>
			</area>
			<area title="Licences Pro" shape="rect" coords="65,281,163,340" alt="Lien vers les Licences Pro">
			  <xsl:attribute name="href"><xsl:value-of select="$absActionURL"/>/<xsl:value-of select="$_lang"/>/liste/licencePro</xsl:attribute>
			</area>
			<area title="Masters pro et Recherche" shape="rect" coords="216,241,354,279" alt="Lien vers les Masters pro et Recherche">
			  <xsl:attribute name="href"><xsl:value-of select="$absActionURL"/>/<xsl:value-of select="$_lang"/>/liste/master</xsl:attribute>
			</area>
			<area title="Master recherche" shape="rect" coords="285,200,354,240" alt="Lien vers les Master recherche">
			  <xsl:attribute name="href"><xsl:value-of select="$absActionURL"/>/<xsl:value-of select="$_lang"/>/liste/master</xsl:attribute>
			</area>
			<area title="Master pro" shape="rect" coords="216,201,284,240" alt="Lien vers les Master pro">
			  <xsl:attribute name="href"><xsl:value-of select="$absActionURL"/>/<xsl:value-of select="$_lang"/>/liste/master</xsl:attribute>
			</area>
			<area title="Doctorats" shape="rect" coords="285,82,353,199" alt="Lien vers les Doctorats">
			  <xsl:attribute name="href"><xsl:value-of select="$absActionURL"/>/<xsl:value-of select="$_lang"/>/liste/DOCT</xsl:attribute>
			</area>
		</map>
          
	</xsl:template><!--FIN INCLUDE OF '../commun/composants/schemaEtudes.cdmfr.xsl'-->
	<!--INCLUDE OF '../commun/composants/domaines.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tDomaines">
			<xsl:param name="pLang"/>
			
		<!-- Recherche par liens directs sur le domaine d'enseignement langue=Français -->
		<xsl:if test="$pLang='fr-FR'">
			<ul>
				<li><a href="{$absActionURL}/{$_lang}/listeDomaine/Arts,+Lettres,+Langues">Domaine Arts, Lettres, Langues (ALL)</a></li>
				<li><a href="{$absActionURL}/{$_lang}/listeDomaine/Sciences+Humaines+et+Sociales">Domaine Sciences Humaines et Sociales (SHS)</a></li>
				<li><a href="{$absActionURL}/{$_lang}/listeDomaine/Droit+Economie+Gestion">Domaine Droit , Economie , Gestion (DEG)</a></li>
				<li><a href="{$absActionURL}/{$_lang}/listeDomaine/Sciences,+Technologies,+Santé">Domaine Sciences, Technologies, Santé (STS)</a></li>
				<li><a href="{$absActionURL}/{$_lang}/listeDomaine/Sciences+et+Techniques+des+Activités+Physiques+et+Sportives">Domaine Sciences et Techniques des <br/>Activités Physiques et Sportives (STAPS)</a></li>
			</ul>
		</xsl:if>
		
		<!-- Recherche par liens directs sur le domaine d'enseignement langue=Anglais -->
		<xsl:if test="$pLang='en-EN'">
			<ul>
				<li><a href="{$absActionURL}/{$_lang}/listeDomaine/Arts,+Lettres,+Langues">Domain Arts, Literature, Languages</a></li>
				<li><a href="{$absActionURL}/{$_lang}/listeDomaine/Sciences+Humaines+et+Sociales">Domain Human and Social Sciences</a></li>
				<li><a href="{$absActionURL}/{$_lang}/listeDomaine/Droit+Economie+Gestion">Domain Law, Economics, Management</a></li>
				<li><a href="{$absActionURL}/{$_lang}/listeDomaine/Sciences,+Technologies,+Santé">Domain Sciences, Technologies, Health</a></li>
				<li><a href="{$absActionURL}/{$_lang}/listeDomaine/Sciences+et+Techniques+des+Activités+Physiques+et+Sportives">Domain Sciences and Techniques<br/>of Physical and Sport Activities</a></li>
			</ul>
		</xsl:if>
		
	</xsl:template><!--FIN INCLUDE OF '../commun/composants/domaines.cdmfr.xsl'-->
	<!--INCLUDE OF '../commun/composants/rechercheAvancee.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tRechercheAvancee">
	
		<form id="rechercheAvancee" action="{$absActionURL}/{$_lang}/postRecherche" method="post">
		  <fieldset>
			<input type="hidden" name="_debut" value="0"/>
			<input type="hidden" name="__1" value="__domaine"/>
			<input type="hidden" name="__2" value="AND"/>
			<input type="hidden" name="__3" value="__diplome"/>
			<input type="hidden" name="__4" value="AND"/>
			<input type="hidden" name="__5" value="__{$_intitule}_pt_AND"/>
			<!-- Les domaines -->
			<div class="ligne_form">
				<label for="__domaine"><xsl:call-template name="get-rub"><xsl:with-param name="rub">43</xsl:with-param></xsl:call-template></label>
				 <select size="1" name="__domaine">
				<option value="">
					<xsl:if test="$domaine=''"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
					<xsl:call-template name="get-rub"><xsl:with-param name="rub">44</xsl:with-param></xsl:call-template>
				</option>
				<xsl:for-each select="/CDM/properties/infoBlock/extension/uniform/constantes/domaineDiplome[@type='domaine']/data">
					<option value="{./@id}">
						<xsl:if test="./@id=$domaine"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
						<xsl:value-of select="./value[@lang=$_lang]"/>
					</option>
				</xsl:for-each>
				</select>
			</div>

			<!-- Les types de diplomes -->
			<div class="ligne_form">
				<label for="__diplome"><xsl:call-template name="get-rub"><xsl:with-param name="rub">40</xsl:with-param></xsl:call-template></label>
				<select size="1" name="__diplome">
					<option value="">
						<xsl:if test="$diplome=''">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>
						<xsl:call-template name="get-rub"><xsl:with-param name="rub">35</xsl:with-param></xsl:call-template>
					</option>
					<xsl:for-each select="/CDM/properties/infoBlock/extension/uniform/constantes/typeDiplome/data">
						 
						  <option value="{./@id}">
							<xsl:if test="./@id=$diplome"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
							<xsl:value-of select="./value[@lang=$_lang]"/>
						  </option>
					</xsl:for-each>
				</select>
			</div>	
			
			<!-- Mots-clés -->
			<div class="ligne_form">
				<label for="__{$_intitule}_pt_AND"><xsl:call-template name="get-rub"><xsl:with-param name="rub">56</xsl:with-param></xsl:call-template></label>
				<input class="entree" size="24" name="__{$_intitule}_pt_AND" type="text">
				  <xsl:attribute name="value"><xsl:choose><xsl:when test="$intitule!=''"><xsl:value-of select="$intitule"/></xsl:when><xsl:otherwise/></xsl:choose></xsl:attribute>
				</input>
			</div>

			<!-- Envoyer la recherche : -->
		   <span class="droit"><xsl:value-of select="$variablesExt/variables/titreVar/titreRub[@type='42']/value[@lang=$_lang]"/><input type="submit" class="bouton" value=""/></span>
		  </fieldset>
		</form>
		
	</xsl:template><!--FIN INCLUDE OF '../commun/composants/rechercheAvancee.cdmfr.xsl'-->
	<!--INCLUDE OF '../commun/composants/accueil.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tAccueil">
	
		<xsl:param name="pLang"/>
		<!-- cas où la langue=Français -->
		<xsl:if test="$pLang='fr-FR'">
			<p>Bienvenue sur le catalogue des formations de xxx<br/>
			</p>
		</xsl:if>
		<!-- cas où la langue=Anglais -->
		<xsl:if test="$pLang='en-EN'">
			<p>Welcome to the Catalog of Formations of xxx<br/>
			</p>
		</xsl:if>
		
	</xsl:template><!--FIN INCLUDE OF '../commun/composants/accueil.cdmfr.xsl'-->
	<xsl:param name="accueil-url">http://portail-ig.univ-lille1.fr/display-content?id=39016</xsl:param>
	<xsl:param name="accessibilite-url">http://portail-ig.univ-lille1.fr/display-content?id=8159</xsl:param>
	<xsl:param name="mentions-legales-url">http://portail-ig.univ-lille1.fr/display-content?id=8158</xsl:param>
	<xsl:template match="/">
		<xsl:call-template name="html"/>
	</xsl:template>
  <xsl:template name="page-script">
  	  <script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"><xsl:text>//
    </xsl:text></script>  
		<script src="{$absActionURL}/media/js/jquery.collapsible-menus.min.js" type="text/javascript"><xsl:text>//
    </xsl:text></script>  
    <script src="{$absActionURL}/media/js/accordion.js" type="text/javascript"><xsl:text>//
    </xsl:text></script> 

	</xsl:template>
	<!-- <xsl:call-template name="get-var"><xsl:with-param name="xpath"></xsl:with-param></xsl:call-template> -->
	<xsl:template name="user-titreFenetre">
		<xsl:call-template name="getVar"><xsl:with-param name="var">0</xsl:with-param></xsl:call-template>
	</xsl:template>
	<xsl:template name="user-navigation">
    <li class="menuHorizontal"> <a href="{$absActionURL}/{$_lang}/liste">Feuilleter le catalogue</a></li>
		<li class="menuHorizontal"><a href="{$absActionURL}/{$_lang}/recherche">Recherche avancée</a></li>
	</xsl:template>
	<xsl:template name="user-titre">
		<h1><xsl:call-template name="get-nvar"><xsl:with-param name="nvar">20</xsl:with-param></xsl:call-template></h1>
		
	</xsl:template>
	<xsl:template name="att-current">
    <xsl:param name="ecran"/>
    <xsl:param name="att"/>
    <xsl:choose>
      <xsl:when test="$ecran='diplome' and $_ecran='di'"><xsl:attribute name="class"><xsl:value-of select="$att"/> current</xsl:attribute></xsl:when>
      <xsl:when test="$ecran='domaine' and $_ecran='do'"><xsl:attribute name="class">current</xsl:attribute></xsl:when>
      <xsl:when test="$ecran='specifique' and $_ecran='sp'"><xsl:attribute name="class">current</xsl:attribute></xsl:when> 
    </xsl:choose>
  </xsl:template>
	<xsl:template name="user-ariane">
    <ol class="breadcrumb">
			<li class="active"><a class="active" href="{$absActionURL}/{$_lang}/accueil"><span class="glyphicon glyphicon-home"><xsl:text> </xsl:text></span><xsl:text> Catalogue des Formations</xsl:text></a></li>
		</ol>
  </xsl:template>
  <xsl:template name="user-ariane-cms">
    <ol class="breadcrumb">
			<li class="active"><a class="active" href="{$absActionURL}/{$_lang}/accueil"><span class="glyphicon glyphicon-home"><xsl:text> </xsl:text></span><xsl:text> Retour catalogue</xsl:text></a></li>
		</ol>
  </xsl:template>
  <xsl:template name="defaut">
    <h2 class="title-lille1">Choisissez votre diplôme</h2>
    <div class="custom-area"><xsl:text> </xsl:text><xsl:copy-of select="/CDM/orgUnit[1]/orgUnitIntroduction"/><!--xsl:value-of select="document($accueil-url)/content/xhtml/text()" disable-output-escaping="yes"/--></div>
    <div class="accordion" id="menu_collapse">
      <h3><xsl:if test="$_ecran='di'"><xsl:attribute name="class">current</xsl:attribute></xsl:if><span class="glyphicon"><xsl:text> </xsl:text></span><xsl:text> </xsl:text>Par type de diplôme (<xsl:value-of select="count(/CDM/program[programDescription/@nature='mention'][qualification/degree/@degree!='specifique'])"/>)</h3>
      <div class="pane"><xsl:if test="$_ecran='di'"><xsl:attribute name="style">display: block;</xsl:attribute></xsl:if>
        <div class="accordion">
          <xsl:apply-templates select="/CDM/properties/infoBlock/extension/uniform/constantes/typeDiplome/data[@id!='specifique']"/>
        </div>
      </div>
      <h3><xsl:if test="$_ecran='do'"><xsl:attribute name="class">current</xsl:attribute></xsl:if><span class="glyphicon"><xsl:text> </xsl:text></span><xsl:text> </xsl:text>Par domaine de formations (<xsl:value-of select="count(/CDM/program[programDescription/@nature='mention'][qualification/degree/@degree!='specifique'])"/>)</h3>
      <div class="pane"><xsl:if test="$_ecran='do'"><xsl:attribute name="style">display: block;</xsl:attribute></xsl:if>
        <div class="accordion">
          <xsl:apply-templates mode="domaine" select="/CDM/properties/infoBlock/extension/uniform/constantes/domaineDiplome/data"/>
        </div>
      </div>
      <h3><xsl:if test="$_ecran='sp'"><xsl:attribute name="class">current</xsl:attribute></xsl:if><span class="glyphicon"><xsl:text> </xsl:text></span><xsl:text> </xsl:text>Par parcours spécifiques de licences(<xsl:value-of select="count(/CDM/program[programDescription/@nature='mention'][qualification/degree/@degree='specifique'])"/>)</h3>
      <div class="pane"><xsl:if test="$_ecran='sp'"><xsl:attribute name="style">display: block;</xsl:attribute></xsl:if>
        <div class="accordion">
          <xsl:apply-templates mode="specifique" select="/CDM/properties/infoBlock/extension/uniform/constantes/domaineDiplome/data"/>
        </div>
      </div>
    </div>
  </xsl:template>
	<xsl:template match="uniform/constantes/typeDiplome/data">
    <xsl:variable name="id" select="@id"/>
    <xsl:if test="/CDM/program[qualification/degree/@degree=$id][@mention='true']">

      <h3><xsl:if test="$diplome=$id"><xsl:attribute name="class">current</xsl:attribute></xsl:if><span class="glyphicon"><xsl:text> </xsl:text></span><xsl:text> </xsl:text><xsl:value-of select="value[@lang=$_lang]"/> <xsl:text> </xsl:text>(<xsl:value-of select="count(/CDM/program[qualification/degree/@degree=$id][@mention='true'])"/>)</h3>
      <div class="pane"><xsl:if test="$id=$diplome"><xsl:attribute name="style">display: block;</xsl:attribute></xsl:if>
          <xsl:apply-templates mode="type" select="/CDM/properties/infoBlock/extension/uniform/constantes/domaineDiplome/data"><xsl:with-param name="type" select="$id"/></xsl:apply-templates>
      </div>

    </xsl:if>
  </xsl:template>
  <xsl:template match="uniform/constantes/typeDiplome/data" mode="domaine">
    <xsl:param name="domaine"/>
    <xsl:variable name="id" select="@id"/>
    <xsl:if test="/CDM/program[programCode/codeDomain=$domaine][@mention='true'][qualification/degree/@degree=$id]">
      <div class="accordion">
        <h3><xsl:if test="$diplome=$id"><xsl:attribute name="class">current</xsl:attribute></xsl:if><span class="glyphicon"><xsl:text> </xsl:text></span><xsl:text> </xsl:text><xsl:value-of select="value[@lang=$_lang]"/><xsl:text> </xsl:text>(<xsl:value-of select="count(/CDM/program[programCode/codeDomain=$domaine][qualification/degree/@degree=$id][@mention='true'])"/>)</h3>
        <ul class="pane submenu"><xsl:if test="$id=$diplome"><xsl:attribute name="style">display: block;</xsl:attribute></xsl:if>
          <xsl:for-each select="/CDM/program[@mention='true'][programCode/codeDomain=$domaine][qualification/degree/@degree=$id]">
                <xsl:sort select="programName/text"/>
                <xsl:apply-templates mode="intitule" select=".">
                  <xsl:with-param name="recherche" select="'do'"/>
                </xsl:apply-templates>
              </xsl:for-each>
          <!--xsl:apply-templates select="/CDM/program[@mention='true'][programCode/codeDomain=$domaine][qualification/degree/@degree=$id]" mode="intitule">
            <xsl:with-param name="recherche" select="'do'"/>
          </xsl:apply-templates-->
        </ul>
      </div>
    </xsl:if>
  </xsl:template>
  <xsl:template match="uniform/constantes/domaineDiplome/data" mode="type">
    <xsl:param name="type"/>
    <xsl:variable name="_domaine" select="@id"/>
    <xsl:if test="/CDM/program[programCode/codeDomain=$_domaine][@mention='true'][qualification/degree/@degree=$type]">
        <div class="accordion">
            <h3 domaine="{$domaine}"><xsl:if test="$domaine=$_domaine"><xsl:attribute name="class">current</xsl:attribute></xsl:if><span class="glyphicon"><xsl:text> </xsl:text></span><xsl:text> </xsl:text><xsl:value-of select="value[@lang=$_lang]"/><xsl:text> </xsl:text>(<xsl:value-of select="count(/CDM/program[programCode/codeDomain=$_domaine][qualification/degree/@degree=$type][@mention='true'])"/>)</h3>
            <ul class="pane submenu"><xsl:if test="$domaine=$_domaine"><xsl:attribute name="style">display: block;</xsl:attribute></xsl:if>
              <xsl:for-each select="/CDM/program[@mention='true'][programCode/codeDomain=$_domaine][qualification/degree/@degree=$type]">
                <xsl:sort select="programName/text"/>
                <xsl:apply-templates mode="intitule" select=".">
                  <xsl:with-param name="recherche" select="'di'"/>
                </xsl:apply-templates>
              </xsl:for-each>
              <!--xsl:apply-templates select="/CDM/program[@mention='true'][programCode/codeDomain=$_domaine][qualification/degree/@degree=$type]" mode="intitule">
                <xsl:with-param name="recherche" select="'di'"/>
              </xsl:apply-templates-->
            </ul>
        </div>
    </xsl:if>
  </xsl:template>
  <xsl:template match="uniform/constantes/domaineDiplome/data">
    <xsl:variable name="_domaine" select="@id"/>
    <xsl:if test="/CDM/program[programCode/codeDomain=$domaine][@mention='true'][qualification/degree/@degree!='specifique']">
      <div class="accordion">
        <h3 domaine="{$domaine}"><xsl:if test="$domaine=$_domaine"><xsl:attribute name="class">current</xsl:attribute></xsl:if><span class="glyphicon"><xsl:text> </xsl:text></span><xsl:text> </xsl:text><xsl:value-of select="value[@lang=$_lang]"/><xsl:text> </xsl:text>(<xsl:value-of select="count(/CDM/program[programCode/codeDomain=$_domaine][@mention='true'][qualification/degree/@degree!='specifique'])"/>)</h3>
        <ul class="pane submenu"><xsl:if test="$domaine=$_domaine"><xsl:attribute name="style">display: block;</xsl:attribute></xsl:if>
          <xsl:apply-templates mode="domaine" select="/CDM/properties/infoBlock/extension/uniform/constantes/typeDiplome/data[@id!='specifique']"><xsl:with-param name="domaine" select="$_domaine"/></xsl:apply-templates>
        </ul>
      </div>
    </xsl:if>
  </xsl:template>
  <xsl:template match="uniform/constantes/domaineDiplome/data" mode="domaine">
    <xsl:variable name="_domaine" select="@id"/>
    <xsl:if test="/CDM/program[programCode/codeDomain=$_domaine][@mention='true'][qualification/degree/@degree!='specifique']">
        <h3 domaine="{$domaine}"><xsl:if test="$domaine=$_domaine"><xsl:attribute name="class">current</xsl:attribute></xsl:if><span class="glyphicon"><xsl:text> </xsl:text></span><xsl:text> </xsl:text><xsl:value-of select="value[@lang=$_lang]"/><xsl:text> </xsl:text>(<xsl:value-of select="count(/CDM/program[programCode/codeDomain=$_domaine][@mention='true'][qualification/degree/@degree!='specifique'])"/>)</h3>
        <div class="pane"><xsl:if test="$domaine=$_domaine"><xsl:attribute name="style">display: block;</xsl:attribute></xsl:if>
          <xsl:apply-templates mode="domaine" select="/CDM/properties/infoBlock/extension/uniform/constantes/typeDiplome/data[@id!='specifique']"><xsl:with-param name="domaine" select="$_domaine"/></xsl:apply-templates>
        </div>
    </xsl:if>
  </xsl:template>
  <xsl:template match="uniform/constantes/domaineDiplome/data" mode="specifique">
    <xsl:variable name="_domaine" select="@id"/>
    <xsl:if test="/CDM/program[programCode/codeDomain=$_domaine][qualification/degree/@degree='specifique'][@mention='true']">
        <h3 domaine="{$domaine}"><xsl:if test="$domaine=$_domaine"><xsl:attribute name="class">current</xsl:attribute></xsl:if><span class="glyphicon"><xsl:text> </xsl:text></span><xsl:text> </xsl:text><xsl:value-of select="value[@lang=$_lang]"/> (<xsl:value-of select="count(/CDM/program[programCode/codeDomain=$_domaine][@mention='true'][qualification/degree/@degree='specifique'])"/>)</h3>
        <ul class="pane submenu"><xsl:if test="$domaine=$_domaine"><xsl:attribute name="style">display: block;</xsl:attribute></xsl:if>
          <xsl:for-each select="/CDM/program[@mention='true'][qualification/degree/@degree='specifique'][programCode/codeDomain=$_domaine]">
            <xsl:sort select="programName/@prefixe"/>
            <xsl:apply-templates mode="intitule" select=".">
              <xsl:with-param name="recherche" select="'sp'"/>
            </xsl:apply-templates>
          </xsl:for-each>
        </ul>
    </xsl:if>
  </xsl:template>


  <xsl:template name="user-col_gauche">	
    <section class="col-sm-9" id="content">
      <xsl:choose>
        <xsl:when test="$_pageIG='mentionsLegales'"><xsl:value-of disable-output-escaping="yes" select="document($mentions-legales-url)/content/xhtml/text()"/></xsl:when>
        <xsl:when test="$_pageIG='accessibilite'"><xsl:value-of disable-output-escaping="yes" select="document($accessibilite-url)/content/xhtml/text()"/></xsl:when>
        <xsl:when test="$_pageIG='accueil'"><xsl:call-template name="defaut"/></xsl:when>
        <xsl:otherwise><xsl:call-template name="defaut"/></xsl:otherwise>
      </xsl:choose>
    </section>
	</xsl:template>
	<xsl:template name="user-col_droite">
      <aside class="col-sm-3">
        <xsl:call-template name="user-recherche"/>
        <xsl:call-template name="user-liens"/>
        <xsl:call-template name="user-devenir_ens"/>
        <xsl:call-template name="user-contact"/>
      </aside>
	</xsl:template>
	<xsl:template name="body"/>
	<xsl:template name="plan-site-google"/>
  
  <!-- les listes de résultats -->
  <xsl:template match="program" mode="intitule">
    <xsl:param name="recherche"/>
    <li>
      <xsl:choose>
        <xsl:when test="webLink[@role='plink']">
          <a href="{webLink[@role='plink']/href}"><xsl:value-of select="programName/text[last()]"/></a> 
        </xsl:when>
        <xsl:otherwise>
          <!--a href="{$absActionURL}/{$_lang}/fiche/presentation/{@id}/{$recherche}/{qualification/degree/@degree}/{programCode/codeDomain}"><xsl:value-of select="programName/text[last()]"/></a-->
          <a href="{$absActionURL}/{$_lang}/fiche/{$recherche}-presentation-{@id}"><xsl:value-of select="programName/text[last()]"/></a>
        </xsl:otherwise>
      </xsl:choose>
    </li>
  </xsl:template>
  <xsl:template name="user-stat">				<!-- Piwik --> 
<script type="text/javascript">
  var _paq = _paq || [];
  (function(){ var u=(("https:" == document.location.protocol) ? "https://webstat.univ-lille1.fr/" : "http://webstat.univ-lille1.fr/");
  _paq.push(['setSiteId', 5]);
  _paq.push(['setTrackerUrl', u+'piwik.php']);
  _paq.push(['setCustomVariable','1','catalogue','accueil', 'page']);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript'; g.defer=true; g.async=true; g.src=u+'piwik.js';
  s.parentNode.insertBefore(g,s); })();
</script><noscript><p><img alt="" src="http://webstat.univ-lille1.fr/piwik.php?idsite=5" style="border:0"/></p></noscript>
<!-- End Piwik Tracking Code -->
</xsl:template>
<xsl:template name="user-stat-cms">
 <script type="text/javascript">
  var titreCatalogue="<xsl:value-of select="$titreCatalogue"/>";
  var res = ""+screen.width+"x"+screen.height ;
  var now = Date.now() ;
  var _url = "<xsl:value-of select="$url"/>"+"/<xsl:value-of select="$_lang"/>/accueil";
  var cvar = '{"1":["catalogue","accueil"]}';
  var base = (("https:" == document.location.protocol) ? "https://webstat.univ-lille1.fr/" : "http://webstat.univ-lille1.fr/");
 <xsl:text disable-output-escaping="yes">
 var theUrl = encodeURI(base+"piwik.php?idsite=5&amp;rec=1&amp;send_image=0&amp;action_name="+titreCatalogue+"&amp;cvar="+cvar+"&amp;url="+_url+"&amp;res="+res+"&amp;urlref="+document.referrer+"&amp;now="+now);

</xsl:text>
  var xmlHttp = null;
  xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", theUrl, false );
  //xmlHttp.setRequestHeader("Content-Type", "image/gif");
  xmlHttp.send( null );
</script>
</xsl:template>
</xsl:stylesheet>