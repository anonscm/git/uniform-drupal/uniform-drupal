<?xml version="1.0" encoding="UTF-8"?><xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="xsl" version="1.0">

	<xsl:output encoding="UTF-8" indent="yes" media-type="text/html" method="xml" omit-xml-declaration="yes" xml:lang="fr" xml:space="preserve"/>

	<!-- FICHIERS INCLUDE UTILISEES   -->
	<!--INCLUDE OF '../commun/composants/commun/commun.xsl'--><xsl:key xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="objet" match="orgUnit | program | subProgram | course | person" use="@id"/><xsl:key xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="noeud" match="noeud" use="@nid"/><xsl:key xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="noeudId" match="noeud" use="@id"/><xsl:key xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="noeudType" match="noeud" use="@typeObjet"/><xsl:key xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="noeudNiveau" match="noeud" use="@niveau"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_lang"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="codeFormation"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="facet"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="baseActionURL"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="absActionURL"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_pas">10</xsl:param><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="debug">0</xsl:param><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="cms">n</xsl:param><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_oid"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_oidSpecialite"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_oidUe"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_redirect"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_onglet"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_profil">amue</xsl:param><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_recherche">result</xsl:param><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="baseMediaURL">media</xsl:param><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_ecran"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="domaine"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="diplome"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="composante"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="title"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="intitule"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="titre-fr"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="intdesc"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="matiere-fr"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="basket" select="0"/><xsl:param xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_debug" select="n"/><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="documentation-uri"><xsl:choose><xsl:when test="$cms='o'">foo.xml</xsl:when><xsl:otherwise>../../../xml2xml/cdmfr-2012-rof-2.50/documentation.xml</xsl:otherwise></xsl:choose></xsl:variable><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="documentation" select="document($documentation-uri)/documentation"/><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="variablesExt-uri"><xsl:choose><xsl:when test="$cms='o'">foo.xml</xsl:when><xsl:otherwise>../../variablesExt.xml</xsl:otherwise></xsl:choose></xsl:variable><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="variablesExt" select="document($variablesExt-uri)"/><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="idP">
		<xsl:choose>
			<xsl:when test="$_oidSpecialite!=''"><xsl:value-of select="$_oidSpecialite"/></xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$_oid"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="mention"><xsl:value-of select="$codeFormation"/></xsl:variable><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="specialite"><xsl:value-of select="$_oidSpecialite"/></xsl:variable><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="ue"><xsl:value-of select="$_oidUe"/></xsl:variable><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="nidP"><xsl:value-of select="key('noeudId',$idP)/@nid"/></xsl:variable><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="langueCDM" select="/CDM/@language"/><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="ongletEnCours" select="normalize-space($_onglet)"/><xsl:variable xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="_intitule">titre-<xsl:value-of select="$_lang"/></xsl:variable><!--INCLUDE OF 'sectionCNU.cdmfr.xsl'--><xsl:template name="tSectionCNU">
		<xsl:param name="pCode"/>
		<xsl:param name="pName"/>
		<xsl:call-template name="tCommunGestionLibelle">
			<xsl:with-param name="pIdent" select="'libSectionCNU'"/>
		</xsl:call-template>
		<xsl:value-of select="@code"/><xsl:text>  </xsl:text><xsl:value-of select="@name"/>
	</xsl:template><!--FIN INCLUDE OF 'sectionCNU.cdmfr.xsl'--><!--INCLUDE OF 'contactData.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="contactData">
		<!-- ajout pour orgUnit -->
		<xsl:if test="not(contactName/text[@language=$_lang]='')">
			<xsl:apply-templates select="contactName/text[@language=$_lang]"/><br/>
		</xsl:if>
		<p>	<xsl:call-template name="tCommunGestionLibelle">
				<xsl:with-param name="pIdent" select="'libAdresse'"/>
			</xsl:call-template>
			<xsl:value-of select="adr/extadr"/><xsl:text>, </xsl:text><xsl:value-of select="adr/street"/><xsl:text>, </xsl:text><xsl:value-of select="adr/pcode"/><xsl:text> </xsl:text><xsl:value-of select="adr/locality"/><xsl:text>, </xsl:text><xsl:value-of select="adr/country"/></p>
		<p>	<xsl:call-template name="tCommunGestionLibelle">
				<xsl:with-param name="pIdent" select="'libTelephoneAbrege'"/>
			</xsl:call-template>
			<xsl:value-of select="telephone"/></p>
		<p>	<xsl:call-template name="tCommunGestionLibelle">
				<xsl:with-param name="pIdent" select="'libTelecopieAbrege'"/>
			</xsl:call-template>
			<xsl:value-of select="fax"/></p>
		<xsl:apply-templates select="email"/>
		<!--<xsl:if test="contains(email,'@')">
			<p><a href="mailto:{email}"><xsl:value-of select="email"/></a></p>
		</xsl:if>-->
		<xsl:if test="infoBlock[@blockLang=$_lang]"> <!-- Complément adresse -->
			<p><xsl:apply-templates select="infoBlock[@blockLang=$_lang]"/></p>
		</xsl:if>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="tContacts">
		<xsl:param name="pNodeContacts"/>
		<!-- section contactData -->
		<xsl:apply-templates select="$pNodeContacts/contactData"/><br/>
		<!-- section refPerson -->
		<xsl:apply-templates select="$pNodeContacts/refPerson"/><br/>
		<!-- section infoBlock -->
		<xsl:apply-templates select="$pNodeContacts/infoBlock"/><br/>
	</xsl:template><!--FIN INCLUDE OF 'contactData.cdmfr.xsl'--><!--INCLUDE OF 'level.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tLevelInfoBlockType">
		<xsl:param name="pNode"/>
		<xsl:value-of select="$pNode/level"/>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tLevelLanguageCECRLLevel">
		<xsl:param name="pNode"/>
		<xsl:value-of select="$pNode/level/@languageCECRLLevel"/>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tLevelLevel">
		<xsl:param name="pNode"/>
		<xsl:if test="$pNode/level/@level">
			<li><xsl:call-template name="tCommunGestionLibelle"><xsl:with-param name="pIdent" select="'libTypeDiplome'"/></xsl:call-template>
				<xsl:choose>
					<xsl:when test="$pNode/level/@level='L'">
						<xsl:value-of select="'Licence'"/>
					</xsl:when>
					<xsl:when test="$pNode/level/@level='M'">
						<xsl:value-of select="'Master'"/>
					</xsl:when>
					<xsl:when test="$pNode/level/@level='Doc'">
						<xsl:value-of select="'Doctorat'"/>
					</xsl:when>
					<xsl:otherwise>INCONNU</xsl:otherwise>
				</xsl:choose>
			</li>
		</xsl:if>
	</xsl:template><!--FIN INCLUDE OF 'level.cdmfr.xsl'--><!--INCLUDE OF 'learningObjectives.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tLearningObjectivesInfoBlockType">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/learningObjectives"/>
	</xsl:template><!--FIN INCLUDE OF 'learningObjectives.cdmfr.xsl'--><!--INCLUDE OF 'admissionInfo.cdmfr.xsl'--><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tAdmissionInfoStudentPlaces">
		<xsl:param name="pNode"/>
		<xsl:value-of select="$pNode/admissionInfo/studentPlaces[@blockLang=$_lang]/@places"/>
		<!--<xsl:value-of select="$pNodeAdmissionInfo/admissionInfo/studentPlaces[@blockLang=$_lang]"/>-->
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tAdmissionInfoAdmissionDescription">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/admissionInfo/admissionDescription"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tAdmissionInfoStudentStatus">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/admissionInfo/studentStatus"/>
	</xsl:template><!--FIN INCLUDE OF 'admissionInfo.cdmfr.xsl'--><!--INCLUDE OF 'recommendedPrerequisites.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tRecommendedPrerequisites">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/recommendedPrerequisites"/>
	</xsl:template><!--FIN INCLUDE OF 'recommendedPrerequisites.cdmfr.xsl'--><!--INCLUDE OF 'formalPrerequisites.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tFormalPrerequisites">
		<xsl:param name="pNode"/>
		<p><xsl:apply-templates select="$pNode/formalPrerequisites"/></p>
	</xsl:template><!--FIN INCLUDE OF 'formalPrerequisites.cdmfr.xsl'--><!--INCLUDE OF 'formOfTeaching.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tFormOfTeaching">
		<xsl:param name="pNode"/>
		<xsl:if test="count($pNode/formOfTeaching) &gt; 0">
			<ul>
				<xsl:for-each select="$pNode/formOfTeaching">
					<li>
						<xsl:apply-templates select="@method"/> : 
						<xsl:apply-templates select="node()"/>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tFormOfTeachingMethod">
		<xsl:param name="pNode"/>
		<xsl:if test="count($pNode/formOfTeaching) &gt; 0">
			<ul>
				<xsl:for-each select="$pNode/formOfTeaching">
					<li>
						<xsl:apply-templates select="@method"/>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tFormOfTeachingInfoBlockType">
		<xsl:param name="pNode"/>
		<xsl:if test="count($pNode/formOfTeaching) &gt; 0">
			<ul>
				<xsl:for-each select="$pNode/formOfTeaching">
					<li>
						<xsl:apply-templates select="node()"/>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tFormOfTeachingInfoBlockTypePDF">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/formOfTeaching"/>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tFormOfTeachingMethodPDF">
		<xsl:param name="pNode"/>
		<xsl:value-of select="$pNode/formOfTeaching/@method"/>
	</xsl:template><!--FIN INCLUDE OF 'formOfTeaching.cdmfr.xsl'--><!--INCLUDE OF 'formOfAssessment.cdmfr.xsl'--><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tFormOfAssessment">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/formOfAssessment"/>
	</xsl:template><!--FIN INCLUDE OF 'formOfAssessment.cdmfr.xsl'--><!--INCLUDE OF 'expenses.cdmfr.xsl'--><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tExpensesInfoBlockType"> 
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/expenses"/>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tExpensesPrice">
		<xsl:param name="pNode"/>
		<xsl:if test="$pNode/expenses/@price"><xsl:value-of select="$pNode/expenses/@price"/><xsl:text> </xsl:text><xsl:value-of select="$pNode/expenses/@currency"/></xsl:if>
	</xsl:template><!--FIN INCLUDE OF 'expenses.cdmfr.xsl'--><!--INCLUDE OF 'credits.cdmfr.xsl'--><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCreditsECTScredits">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/credits/@ECTScredits"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCreditsInfoBlockType">
		<xsl:param name="pNode"/>
		<xsl:apply-templates select="$pNode/credits/infoBlock"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCreditsGlobalVolume">
		<xsl:param name="pNode"/>
		<ul>
			<xsl:for-each select="$pNode/credits/globalVolume">
				<li>
					<xsl:call-template name="transformTeachingtype"><xsl:with-param name="pTeachingtype" select="@teachingtype"/></xsl:call-template> :
					<xsl:apply-templates select="node()"/>
					<xsl:text> h</xsl:text>
				</li>
			</xsl:for-each>
		</ul>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCreditsGlobalVolumeTeachingType">
		<xsl:param name="pNode"/>
		<xsl:param name="pTeachingType"/><xsl:value-of select="$pNode/credits"/>
		<xsl:apply-templates select="$pNode/credits/globalVolume[@teachingtype=$pTeachingType]/node()"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="transformTeachingtype">
		<xsl:param name="pTeachingtype"/>
		<xsl:choose>
			<xsl:when test="$pTeachingtype = 'homeWork'">
				<xsl:value-of select="$variablesExt/variables/courseVar/userDefine[@type='volumeHomeWork']/value[@lang=$_lang]"/>
			</xsl:when>
			<xsl:when test="$pTeachingtype = 'ProfessTraining'">
				<xsl:value-of select="$variablesExt/variables/courseVar/userDefine[@type='volumeProfessTraining']/value[@lang=$_lang]"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$pTeachingtype"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template><!--FIN INCLUDE OF 'credits.cdmfr.xsl'--><!--INCLUDE OF 'natureElements.cdmfr.xsl'--><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUneMention">
	<!-- Cette variable locale permet de déterminer si l'on est sur une mention ou pas (pour conditionner l'affichage) -->
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureProgramID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,2)='ME'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUneSpecialite">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureProgramID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,2)='SP'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUnProgramme">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureProgramID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,2)='PR'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUnComposantProgramme">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureProgramID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,2)='CP'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUneAnnee">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureProgramID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,4)='CPAN'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUnSemestre">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureProgramID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,4)='CPSE'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUnEnseignement">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureEnseignementID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,2)='EN'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUneUE">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureEnseignementID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,4)='ENUE'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="vBoolEstUneEC">
		<xsl:variable name="vProgramID">
			<xsl:call-template name="tNatureEnseignementID"/>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="substring($vProgramID,16,4)='ENEC'"><xsl:value-of select="true()"/></xsl:when>
			<xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
		</xsl:choose>
	</xsl:variable><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tNatureProgramID">
		<xsl:apply-templates select="key('objet',$idP)/programID"/>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tNatureEnseignementID">
		<xsl:value-of select="key('objet',$idP)/courseID"/>
	</xsl:template><!--FIN INCLUDE OF 'natureElements.cdmfr.xsl'--><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="get-var">
		<xsl:param name="xpath"/>
    <xsl:param name="location" select="'indéfinie'"/>
		<xsl:choose><xsl:when test="$xpath"><xsl:value-of select="$xpath"/></xsl:when><xsl:otherwise>INDEFINI<img src="{$baseMediaURL}/images/help.png" alt="location={$location}" title="location={$location}"/></xsl:otherwise></xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="get-lib">
		<xsl:param name="lib"/>
    <xsl:param name="location" select="'indéfinie'"/>
    
		<xsl:choose><xsl:when test="$variablesExt/variables/libelleVar/libellePart[@type=$lib]/value[@lang=$_lang]"><xsl:value-of select="$variablesExt/variables/libelleVar/libellePart[@type=$lib]/value[@lang=$_lang]"/></xsl:when><xsl:otherwise><xsl:value-of select="$lib"/><img src="{$baseMediaURL}/images/help.png" alt="location={$location}" title="location={$location}"/></xsl:otherwise></xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="getVar">
		<xsl:param name="var"/>
    <xsl:param name="location" select="'indéfinie'"/>
		<xsl:choose><xsl:when test="$variablesExt/root">CMS</xsl:when><xsl:when test="$variablesExt/variables/pageTitle/header[@type=$var]/value[@lang=$_lang]"><xsl:value-of select="$variablesExt/variables/pageTitle/header[@type=$var]/value[@lang=$_lang]"/></xsl:when><xsl:otherwise><xsl:value-of select="$var"/><img src="{$baseMediaURL}/images/help.png" alt="location={$location}" title="location={$location}"/></xsl:otherwise></xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="get-nvar">
		<xsl:param name="nvar"/>
    <xsl:param name="location" select="'indéfinie'"/>
		<xsl:choose><xsl:when test="$variablesExt/variables/navigationVar/link[@type=$nvar]/navigationItem/value[@lang=$_lang]"><xsl:value-of select="$variablesExt/variables/navigationVar/link[@type=$nvar]/navigationItem/value[@lang=$_lang]"/></xsl:when><xsl:otherwise><xsl:value-of select="$nvar"/><img src="{$baseMediaURL}/images/help.png" alt="location={$location}" title="location={$location}"/></xsl:otherwise></xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="get-rub">
		<xsl:param name="rub"/>
    <xsl:param name="location" select="'indéfinie'"/>
		<xsl:choose><xsl:when test="$variablesExt/variables/titreVar/titreRub[@type=$rub]/value[@lang=$_lang]"><xsl:value-of select="$variablesExt/variables/titreVar/titreRub[@type=$rub]/value[@lang=$_lang]"/></xsl:when><xsl:otherwise><xsl:value-of select="$rub"/><img src="{$baseMediaURL}/images/help.png" alt="location={$location}" title="location={$location}"/></xsl:otherwise></xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tUser-col_droite">
		<xsl:param name="pNom"/>
		<xsl:param name="pContactData"/>
		<xsl:param name="pLienLibelle"/>
		<xsl:param name="pLienURL"/>
		<xsl:if test="not($pNom ='')">
			<h3><xsl:value-of select="$pNom"/></h3>
		</xsl:if>
		<xsl:if test="not($pContactData ='')">
			<xsl:apply-templates select="/CDM/orgUnit[1]/contacts/contactData"/>
		</xsl:if>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="webLink" mode="enSavoirPlus">
		<xsl:choose>
			<xsl:when test="not(normalize-space(href))='' and not(normalize-space(linkName))=''"><p><a href="{href}"><xsl:value-of select="linkName"/></a></p></xsl:when>
			<xsl:when test="not(normalize-space(href))=''"><p><a href="{href}"><xsl:value-of select="href"/></a></p></xsl:when>
		</xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tLienEnSavoirPlus">
		<xsl:param name="pLienURL"/>
		<xsl:if test="not(normalize-space($pLienURL) ='')">
			<a href="{$pLienURL}"><xsl:value-of select="$pLienURL"/></a>
		</xsl:if>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="searchword[position()=1]">
		<xsl:value-of select="text()"/>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="searchword[position()&gt;1]"><xsl:text>, </xsl:text><xsl:value-of select="text()"/>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="lov[@info='erasmus']">
		<li>[<xsl:value-of select="@code"/>] <xsl:value-of select="text()"/></li>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="lov[@info='rome']">
		<li>[<xsl:value-of select="@code"/>] <xsl:value-of select="text()"/></li>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCommunGestionLibelle">
		<xsl:param name="pIdent"/>
		<xsl:param name="idDoc" select="'vide'"/>
		<xsl:call-template name="tCommunDoc"><xsl:with-param name="idDoc" select="$idDoc"/></xsl:call-template>
    <xsl:call-template name="get-lib">
      <xsl:with-param name="lib"><xsl:value-of select="normalize-space($pIdent)"/></xsl:with-param>
    </xsl:call-template>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCommunDoc">
		<xsl:param name="idDoc" select="'vide'"/>
		<xsl:param name="element" select="$documentation/element[@id=$idDoc]"/>
		<xsl:if test="$element/@id and $_debug='y'">
			<a class="documentation" href="">?</a>
			<div class="documentation" style="display:none">
				<ul>
					<xsl:choose>
						<xsl:when test="$element/@cdmObjet='uniform'"><xsl:apply-templates select="$element"><xsl:with-param name="objet" select="/CDM/properties/infoBlock/extension/uniform"/></xsl:apply-templates></xsl:when>
						<xsl:when test="$element/@cdmObjet='properties'"><xsl:apply-templates select="$element"><xsl:with-param name="objet" select="/CDM/properties"/></xsl:apply-templates></xsl:when>
						<xsl:when test="$element/@cdmObjet='habilitation'"><xsl:apply-templates select="$element"><xsl:with-param name="objet" select="key('objet',$idP)/habilitation"/></xsl:apply-templates></xsl:when>
						<xsl:otherwise><xsl:apply-templates select="$element"><xsl:with-param name="objet" select="key('objet',$idP)"/></xsl:apply-templates></xsl:otherwise>
					</xsl:choose>
				</ul>
			</div>
		</xsl:if>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="*" mode="transform">&lt;<xsl:value-of select="name()"/> <xsl:apply-templates select="@*" mode="transform"/><xsl:if test="not(*|text())">/</xsl:if>&gt;<xsl:if test="*|text()"><xsl:apply-templates select="*|text()" mode="transform"/>&lt;/<xsl:value-of select="name()"/>&gt;</xsl:if></xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="@*" mode="transform"><xsl:text> </xsl:text><xsl:value-of select="name()"/>="<xsl:value-of select="."/>"</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="text()" mode="transform"><xsl:value-of select="."/></xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="*|@*" mode="debug">
		<xsl:param name="path"/>
		<xsl:choose>
			<xsl:when test="local-name()=$path or $path=''"><xsl:apply-templates select="." mode="transform"/></xsl:when>
			<xsl:otherwise><xsl:apply-templates select="*|@*" mode="debug"><xsl:with-param name="path" select="$path"/></xsl:apply-templates></xsl:otherwise>
		</xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="element">
		<xsl:param name="objet"/>
		<xsl:variable name="xpath">
			<xsl:if test="@xpath">
				<pre>
					<xsl:apply-templates select="$objet" mode="debug">
						<xsl:with-param name="path" select="@xpath"/>
					</xsl:apply-templates>
				</pre>
			</xsl:if>
		</xsl:variable>
		<li>
			<p><xsl:value-of select="@label"/> 
				<xsl:choose><xsl:when test="@href and @value"> : <a href="{@href}"><xsl:value-of select="@value"/></a></xsl:when><xsl:when test="@value"> : <xsl:value-of select="@value"/></xsl:when><xsl:otherwise/></xsl:choose>
				<xsl:text> </xsl:text><xsl:if test="@cdmObjet">Objet : "<xsl:value-of select="@cdmObjet"/>"</xsl:if>
			</p>
			<xsl:value-of select="$idP"/>
			<xsl:if test="element">
				<ul>
					<xsl:apply-templates select="element"/>
				</ul>
			</xsl:if>
			<xsl:apply-templates select="p"/>
			<xsl:copy-of select="$xpath"/>
		</li>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="p">
		<xsl:copy-of select="."/>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="adr">
		<xsl:if test="string-length(pobox)+string-length(extadr)+string-length(street)+string-length(pcode)+string-length(locality)&gt;0">
			<p>
				<xsl:if test="string-length(pobox)&gt;0"><xsl:value-of select="pobox"/>,<xsl:text> </xsl:text></xsl:if>
				<xsl:if test="string-length(extadr)&gt;0"><xsl:value-of select="extadr"/>,<xsl:text> </xsl:text></xsl:if> 
				<xsl:if test="string-length(street)&gt;0"><xsl:value-of select="street"/></xsl:if><xsl:if test="string-length(pcode)+string-length(locality)&gt;0"><xsl:text>, 
				</xsl:text><xsl:value-of select="pcode"/><xsl:text> </xsl:text><xsl:value-of select="locality"/></xsl:if>
			</p>
		</xsl:if>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="ariane2">
		<xsl:apply-templates select="/CDM/properties[@facet=$facet]/infoBlock/extension/hierarchy/noeud" mode="ariane2"/>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="noeud" mode="ariane2">
		&gt; <xsl:call-template name="construireLien"><xsl:with-param name="noeud" select="."/></xsl:call-template>
		<xsl:if test="@nid!=$nidP"><xsl:apply-templates select="noeud[descendant-or-self::noeud/@nid=$nidP][1]" mode="ariane2"/></xsl:if>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="construireLien">
		<xsl:param name="noeud"/>
		<xsl:variable name="ecran">fiche</xsl:variable>
		<xsl:variable name="onglet">description</xsl:variable>
		
		<xsl:choose>
			<xsl:when test="$nidP!=$noeud/@nid">
				<xsl:choose>
					<xsl:when test="$noeud/@typeObjet!='course'">
						<!-- cas gÃ©nÃ©ral : Parcours, annÃ©e, semestres -->
						<a href="{$absActionURL}/{$_lang}/{$ecran}/programme/{$mention}/{$noeud/@id}">
							<xsl:text> </xsl:text><xsl:value-of select="$noeud/@libelle"/><xsl:text> </xsl:text>
						</a>
					</xsl:when>
					<xsl:otherwise>
						<a href="{$absActionURL}/{$_lang}/{$ecran}/programme/{$mention}/{$noeud/@id}#ficheEC">
							<xsl:text> </xsl:text><xsl:value-of select="$noeud/@libelle"/><xsl:text> </xsl:text>
						</a>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text> </xsl:text><xsl:value-of select="$noeud/@libelle"/><xsl:text> </xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="listeElementsDossier">
		<ul type="none">
			<xsl:apply-templates select="/CDM/properties[@facet=$facet]/infoBlock/extension/hierarchy/noeud" mode="listeElementsDossier"/>
		</ul>
	</xsl:template><xsl:template xmlns:exsl="http://exslt.org/common" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="noeud" mode="listeElementsDossier">
		<!-- Cas ou tout est depliÃ© -->
		<xsl:choose>
			<xsl:when test="$_oidUe = 'deplier'">
				<li>
					<xsl:call-template name="construireLien"><xsl:with-param name="noeud" select="."/></xsl:call-template>
					<xsl:if test="noeud">
						<ul>
							<xsl:apply-templates select="noeud" mode="listeElementsDossier"/>
						</ul>	
					</xsl:if>
				</li>
			</xsl:when>
			<xsl:when test="$_oidUe = 'replier'">
				<li style="list-style-type:none">
					<xsl:if test="noeud[position()!=last()]">
						<a href="javascript:void(0);" onclick="show_hide('bloc_{@nid}','img_{@nid}', '{$baseMediaURL}');">
							<img id="img_{@nid}" src="{$baseMediaURL}/images/deplier.jpg" border="0"/>
						</a>
					</xsl:if>
					<xsl:call-template name="construireLien"><xsl:with-param name="noeud" select="."/></xsl:call-template>
					<xsl:if test="noeud">
						<ul>
							<xsl:choose>
								<xsl:when test="noeud[position() &gt; 1]">
									<div id="bloc_{@nid}" style="display:none" class="pliage">
										<xsl:apply-templates select="noeud" mode="listeElementsDossier"/>
									</div>
								</xsl:when>
								<xsl:otherwise>
									<div id="bloc_{@nid}" class="pliage">
										<xsl:apply-templates select="noeud" mode="listeElementsDossier"/>
									</div>
								</xsl:otherwise>
							</xsl:choose>
						</ul>
					</xsl:if>
				</li>
			</xsl:when>
			<xsl:otherwise>
				<li style="list-style-type:none">
					<xsl:choose>
						<!-- 1er cas : on est dans l'arborescence : affichage des noeuds parents -->
						<xsl:when test="noeud[descendant-or-self::noeud/@nid=$nidP][1]">
							<xsl:if test="noeud[position()!=last()]">
								<a href="javascript:void(0);" onclick="show_hide('bloc_{@nid}','img_{@nid}', '{$baseMediaURL}');">
									<img id="img_{@nid}" src="{$baseMediaURL}/images/plier.jpg" border="0"/>
								</a>
							</xsl:if>
							<xsl:call-template name="construireLien"><xsl:with-param name="noeud" select="."/></xsl:call-template>
								<ul>
									<div id="bloc_{@nid}" class="pliage"><xsl:apply-templates select="noeud" mode="listeElementsDossier"/>
									</div>
								</ul>
						</xsl:when>
						<!-- Sinon, on est au niveau de la mention -->
						<xsl:otherwise>
							<xsl:if test="noeud[position()!=last()]">
								<a href="javascript:void(0);" onclick="show_hide('bloc_{@nid}','img_{@nid}', '{$baseMediaURL}');">
									<img id="img_{@nid}" src="{$baseMediaURL}/images/deplier.jpg" border="0"/>
								</a>
							</xsl:if>
							<xsl:call-template name="construireLien"><xsl:with-param name="noeud" select="."/></xsl:call-template>
							<xsl:if test="noeud">
								<ul>
									<xsl:choose>
										<xsl:when test="noeud[position() &gt; 1]">
											<div id="bloc_{@nid}" style="display:none" class="pliage">
												<xsl:apply-templates select="noeud" mode="listeElementsDossier"/>
											</div>
										</xsl:when>
										<xsl:otherwise>
											<div id="bloc_{@nid}" class="pliage">
												<xsl:apply-templates select="noeud" mode="listeElementsDossier"/>
											</div>
										</xsl:otherwise>
									</xsl:choose>
								</ul>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</li>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template><!--FIN INCLUDE OF '../commun/composants/commun/commun.xsl'-->
	<!--INCLUDE OF '../lille1-1.0/xhtml.xsl'--><xsl:param xmlns="http://www.w3.org/1999/xhtml" name="url">http://formations.univ-lille1.fr</xsl:param><xsl:param xmlns="http://www.w3.org/1999/xhtml" name="titreCatalogue">Catalogue des formations de l'université Lille 1 - Sciences et technologies</xsl:param><!--INCLUDE OF 'user-xhtml.xsl'--><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="ariane">
		<xsl:choose><xsl:when test="$idP=$_oid"><xsl:value-of select="key('objet', $_oid)/programName/text[@language=$_lang]"/></xsl:when><xsl:otherwise><a href="{$absActionURL}/{$_lang}/fiche/description/{$_oid}"><xsl:value-of select="key('objet', $_oid)/programName/text[@language=$_lang]"/></a> &gt; <xsl:value-of select="substring-after(key('objet', $idP)/programName/text[@language=$_lang],' ')"/></xsl:otherwise></xsl:choose>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-titreCatalogue">

  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-recherche">
    <section id="recherche" class="clearfix">
					<h3 class="title-lille1">Recherche</h3>
					<form role="form" action="{$absActionURL}/{$_lang}/liste" method="post">
          				<input type="hidden" name="_debut" value="0"/>        
                  <input type="hidden" name="_pas" value="200"/>
                  <input type="hidden" name="__1" value="__domaine"/>
                  <input type="hidden" name="__2" value="AND"/>
                  <input type="hidden" name="__3" value="__diplome"/>
                  <input type="hidden" name="__4" value="AND"/>
                  <input type="hidden" name="__5" value="__composante"/>
                  <input type="hidden" name="__6" value="AND"/>
                  <input type="hidden" name="__7" value="__matiere-fr_pt_AND"/>
                  <input type="hidden" name="__8" value="AND"/>
                  <input type="hidden" name="__9" value="__titre-fr_pt_AND"/>
                  <input type="hidden" name="_rechercheActive" value="o"/>
						
            <div class="form-group">
							<label for="search-diplome" class="sr-only">Diplôme</label>
							<select class="form-control input-sm" id="search-diplome" size="1" name="__diplome">
								<option value=""><xsl:if test="$diplome=''"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>par diplôme...</option>
                <xsl:for-each select="/CDM/properties/infoBlock/extension/uniform/constantes/typeDiplome/data">
                  <option value="{./@id}">
                    <xsl:if test="./@id=$diplome"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
                    <xsl:value-of select="./value[@lang=$_lang]"/>
                  </option>
                </xsl:for-each>
							</select>
						</div>
            <div class="form-group">
							<label for="search-domaine" class="sr-only">Domaine</label>
							<select class="form-control input-sm" id="search-domaine" size="1" name="__domaine">
								<option value=""><xsl:if test="$domaine=''"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>Par domaine...</option>
                <xsl:for-each select="/CDM/properties/infoBlock/extension/uniform/constantes/domaineDiplome[@type='domaine']/data">
                  <option value="{./@id}">
                    <xsl:if test="./@id=$domaine"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
                    <xsl:value-of select="./value[@lang=$_lang]"/>
                  </option>
                </xsl:for-each>
							</select>
						</div>
						<div class="form-group">
							<label for="search-composante" class="sr-only">Composante</label>
							<select class="form-control input-sm" id="search-composante" size="1" name="__composante">
                <option value=""><xsl:if test="$composante=''"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>Toutes les composantes</option>		
                <xsl:for-each select="/CDM/orgUnit/orgUnit">
                  <xsl:sort select="orgUnitName/text"/>
                  <option value="{./@ident}">
                    <xsl:if test="./@ident=$composante"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
                    <xsl:value-of select="orgUnitName/text"/>
                  </option>
                </xsl:for-each>
							</select>
						</div>
						<div class="form-group">
							<label for="search-matieres" class="sr-only">Matières enseignées</label>
							<select class="form-control input-sm" id="search-matieres" size="1" name="__matiere-fr_pt_AND">
								<option value=""><xsl:if test="$matiere-fr=''"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>Toutes les matières</option>
                <xsl:for-each select="/CDM/properties/infoBlock/extension/uniform/constantes/matiere/data">
                  <option value="{./value[@lang=$_lang]}">
                    <xsl:if test="./value[@lang=$_lang]=$matiere-fr"><xsl:attribute name="selected">selected</xsl:attribute></xsl:if>
                    <xsl:value-of select="./value[@lang=$_lang]"/>
                  </option>
                </xsl:for-each>
							</select>
						</div>
						<div class="form-group">
							<label for="search-keywords" class="sr-only">Mots-clés</label>
              <input id="search-keywords" name="__titre-{$_lang}_pt_AND" type="text" class="form-control" placeholder="mots-clés...">
                <xsl:attribute name="value"><xsl:value-of select="$titre-fr"/></xsl:attribute>
              </input>
						</div>
						<button type="submit" class="btn btn-default pull-right"><span class="glyphicon glyphicon-search"><xsl:text> </xsl:text></span> Rechercher</button>
					</form>
				</section>
 	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" match="webLink" mode="accueil">
    <li><a href="{href}"><xsl:value-of select="linkName"/></a></li>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-liens">
    <section id="links">
      <h3 class="title-lille1">Liens utiles</h3>
      <ul class="list-unstyled">
        <xsl:apply-templates select="/CDM/orgUnit[1]/webLink[@role='link']" mode="accueil"/>
      </ul>
    </section>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-devenir_ens">
    <!--section id="devenir_ens">
      <h3 class="title-lille1">Devenir enseignant</h3>
      <ul class="list-unstyled">
        <li><a href="http://www.univ-lille1.fr/etudes/Organisation-etudes/Preparations_concours_enseignement">Devenir enseignant : ce qui change à la rentrée 2013 !</a></li>
        <li><a href="http://www-espe-lille-norddefrance.lille.iufm.fr/">ESPE Lille Nord de France</a></li>
      </ul>
    </section-->
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-contact">
    <section id="contact">
      <h3 class="title-lille1">Contact : </h3>
      <ul class="list-unstyled">
        <li><a href="{/CDM/orgUnit[1]/webLink[@role='mail'][1]/href}"><xsl:value-of select="/CDM/orgUnit[1]/webLink[@role='mail'][1]/linkName"/></a></li>
      </ul>
    </section>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-langue">

	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" match="uniform/constantes/facet/language/data">
		<span><a href="{$absActionURL}/{@id}/accueil"><xsl:value-of select="value[@lang=$_lang]"/></a> </span>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-pied">
		<footer class="center-page">
      <hr class="visible-print"/>
      <p class="visible-print">Université Lille1 Sciences et Technologies</p>
      <p>Cité Scientifique 59655 Villeneuve d'Ascq Cedex  Tél. +33 (0) 3.20.43.43.43 <br/>
        <a class="screen" href="{$absActionURL}/{$_lang}/accueil/mentionsLegales" accesskey="8">Mentions légales</a>
      </p>
      
    </footer>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-css">
		<!-- custom Lille1 -->
    <link href="{$absActionURL}/media/css/main.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="{$absActionURL}/media/css/print.css" rel="stylesheet" type="text/css" media="print"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="user-scripts">
	  <!-- ordre de chargement IMPORTANT -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"><xsl:text>//
    </xsl:text></script>   
    <script src="{$absActionURL}/media/bootstrap/js/bootstrap.min.js" type="text/javascript"><xsl:text>//
    </xsl:text></script>   
    
    <xsl:call-template name="page-script"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" match="linkItem">
    <option value="{itemValue}"><xsl:value-of select="itemName"/></option>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" match="groupItems">
    <optgroup label="{@value}">
      <xsl:apply-templates/>
    </optgroup>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="plan-site">
		<ul>
			<li><a href="{$absActionURL}/{$_lang}/accueil">Accueil</a></li>
			<li><a href="{$absActionURL}/{$_lang}/liste">Feuilleter le catalogue</a></li>
			<li><a href="{$absActionURL}/{$_lang}/recherche">Recherche avancée</a></li>
			<li><a href="{$absActionURL}/{$_lang}/accueil/accessibilite">Accessibilité</a></li>
			<li><a href="{$absActionURL}/{$_lang}/accueil">Mentions légales</a></li>
			<li><a href="{$absActionURL}/{$_lang}/plan-site">Plan du Site</a></li>
		</ul>
	</xsl:template><!--FIN INCLUDE OF 'user-xhtml.xsl'--><!--INCLUDE OF 'user-contacts.xsl'--><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="contact-suaio">
	<div class="subinfo">
		<h4>Orientation - Information</h4>
		<span class="image"><img src="{$absActionURL}/media/images/mel.png" alt="Email : "/><a href="mailto:suaio@univ-lille1.fr" title="Lien vers le mail de suaio@univ-lille1.fr">suaio@univ-lille1.fr</a></span>
		<span class="image"><img src="{$absActionURL}/media/images/tel.png" alt="Tel : "/>+33 (0) 320 05 87 49</span>
		<span class="image"><img src="{$absActionURL}/media/images/link.png" alt="Lien : "/><a href="http://suaio.univ-lille1.fr/" onclick="return nleFenetre('http://suaio.univ-lille1.fr/')" title="Lien vers site web SUAIO">site web SUAIO</a></span>
	</div>
</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="contact-admission">
	<div class="subinfo">
		<h4>Admission - Inscription</h4>
		<span class="image"><img src="{$absActionURL}/media/images/link.png" alt="Lien : "/><a href="http://www.univ-lille1.fr/etudes/Admission-Inscription" onclick="return nleFenetre('http://www.univ-lille1.fr/etudes/Admission-Inscription')" title="Lien vers site web Admission">site web Admission</a></span>
	</div>
</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="contact-vie">
	<div class="subinfo">
		<h4>Vie étudiante</h4>
		<span class="image"><img src="{$absActionURL}/media/images/mel.png" alt="Email : "/><a href="http://www.univ-lille1.fr/campus/" onclick="return nleFenetre('http://www.univ-lille1.fr/campus/')" title="Lien vers Site web Vie étudiante">Site web Vie étudiante</a></span>
	</div>
</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="contact-poursuites">
	<div class="subinfo">
		<h4>Poursuites d'études - Insertion professionnelle</h4>
		<span class="image"><img src="{$absActionURL}/media/images/mel.png" alt="Email : "/><a href="mailto:pass-pro@univ-lille1.fr" title="Lien vers le mail de pass-pro@univ-lille1.fr">pass-pro@univ-lille1.fr</a></span>
		<span class="image"><img src="{$absActionURL}/media/images/link.png" alt="Lien : "/><a href="http://pass-pro.univ-lille1.fr/" onclick="return nleFenetre('http://pass-pro.univ-lille1.fr/')" title="Lien vers Pass'Pro : Bureau d'Aide à l'Insertion Professionnelle">Pass'Pro : Bureau d'Aide à l'Insertion Professionnelle</a></span>
		<br/><br/>
		<span class="image"><img src="{$absActionURL}/media/images/mel.png" alt="Email : "/><a href="mailto:ofip@univ-lille1.fr" title="Lien vers le mail de ofip@univ-lille1.fr">ofip@univ-lille1.fr</a></span>
		<span class="image"><img src="{$absActionURL}/media/images/link.png" alt="Lien : "/><a href="http://ofip.univ-lille1.fr/" onclick="return nleFenetre('http://ofip.univ-lille1.fr/')" title="Lien vers OFIP : Observatoire des Formations et de l'Insertion Professionnelle">Observatoire des Formations et de l'Insertion Professionnelle</a></span>
		<br/><br/>
		<span class="image"><img src="{$absActionURL}/media/images/mel.png" alt="Email : "/><a href="mailto:suaio@univ-lille1.fr" title="Lien vers le mail de suaio@univ-lille1.fr">suaio@univ-lille1.fr</a></span>
		<span class="image"><img src="{$absActionURL}/media/images/link.png" alt="Lien : "/><a href="http://suaio.univ-lille1.fr/" onclick="return nleFenetre('http://suaio.univ-lille1.fr/')" title="Lien vers SUAIO : Service Universitaire d'Accueil Information Orientation">Service Universitaire d'Accueil Information Orientation</a></span>
	</div>
</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="contact-ri">
	<div class="subinfo">
		<h4>Relations internationales</h4>
		<span class="image"><img src="{$absActionURL}/media/images/link.png" alt="Lien : "/><a href="http://ci.univ-lille1.fr" onclick="return nleFenetre('http://ci.univ-lille1.fr')" title="Lien vers site web Centre International">Site web Centre International</a></span>
	</div>
</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="refPerson" mode="role">
		<xsl:apply-templates select="key('objet',@ref)" mode="role">
			<xsl:with-param name="role" select="@role"/>
		</xsl:apply-templates>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="person" mode="role">
		<xsl:param name="role"/>
		<xsl:variable name="leRole">
			<xsl:if test="$role!=''">
				<xsl:value-of select="normalize-space($role)"/>
			</xsl:if>
			<xsl:if test="$role=''">
				<xsl:value-of select="normalize-space(./role)"/>
			</xsl:if>
		</xsl:variable>
		<div class="subinfo">
			<h4><xsl:value-of select="$leRole"/></h4>
			<xsl:apply-templates select="./contactData" mode="contact"/>
		</div>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="extadr" mode="contact">
		<xsl:if test="not(normalize-space(.)='')">
			<span class="image"><img src="{$absActionURL}/media/images/adr.png" alt="Adresse : "/>
				<xsl:value-of select="."/>
			</span>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="contactData" mode="contact">
		<!-- Traitement mail vide -->
		<xsl:variable name="nom">
			<xsl:choose>
				<xsl:when test="not(normalize-space(../name/family)='')">
					<xsl:value-of select="../name/family"/>
				</xsl:when>
				<xsl:when test="email">
					<xsl:value-of select="./email"/>
				</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="not(./email='')">
				<span class="image"><img src="{$absActionURL}/media/images/mel.png" alt="Email : "/><a href="mailto:{email}" title="adresse électronique"><xsl:value-of select="$nom"/></a></span>
			</xsl:when>
			<xsl:otherwise>
				<span><xsl:value-of select="$nom"/></span>
			</xsl:otherwise>
		</xsl:choose>
		<!-- Traitement telephone et Fax vide -->
		<xsl:if test="not(normalize-space(./telephone)='')">
			<span class="image"><img src="{$absActionURL}/media/images/tel.png" alt="Tel : "/>
				<xsl:value-of select="./telephone"/>
			</span>
		</xsl:if>
		<xsl:if test="not(normalize-space(./fax)='')">
			<span class="image"><img src="{$absActionURL}/media/images/fax.png" alt="Fax : "/>
				<xsl:value-of select="./fax"/>
			</span>
		</xsl:if>
		<xsl:if test="not(normalize-space(./webLink)='')">
			<xsl:apply-templates select="./webLink">
				<xsl:with-param name="lienExterieur" select="1"/>
				<xsl:with-param name="menu" select="1"/>
			</xsl:apply-templates>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="plan-recherche"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="user-link"/><!--FIN INCLUDE OF 'user-contacts.xsl'--><!--INCLUDE OF 'user-commun.xsl'--><xsl:key xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="program" match="/CDM/properties/infoBlock/extension/uniform/constantes/catalogue//program" use="@id"/><!--FIN INCLUDE OF 'user-commun.xsl'--><xsl:param xmlns="http://www.w3.org/1999/xhtml" name="urlSites"><xsl:choose><xsl:when test="$cms='o'">foo.xml</xsl:when><xsl:otherwise>http://www.univ-lille1.fr/composants/acces_directs_xml</xsl:otherwise></xsl:choose></xsl:param><xsl:variable xmlns="http://www.w3.org/1999/xhtml" name="redirections" select="document($urlSites)/selectItems"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="html">
    <xsl:choose>
      <xsl:when test="$cms='o'"><xsl:call-template name="html-cms"/></xsl:when>
      <xsl:otherwise>
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
        <html lang="fr">
          <head>
            <xsl:call-template name="html-header"/>
          </head>
          <body>
            <header>
              <xsl:call-template name="centre-accessibilite"/>
              <xsl:call-template name="acces"/>
            </header>
            <xsl:call-template name="html-body"/>
            <xsl:call-template name="user-pied"/>
            <xsl:call-template name="user-scripts"/>
            <xsl:call-template name="user-stat"/>
          </body>
        </html>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="html-cms">
				<xsl:call-template name="html-body-cms"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="html-header">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    	<!-- responsive IE/iOS -->
    	<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    	<meta name="viewport" content="width=device-width, initial-scale=1"/>
		<title>
			<xsl:call-template name="user-titreFenetre"/>
		</title>
    <link rel="icon" href="{$baseMediaURL}/images/favicon.ico"/>
    <!-- Bootstrap -->
    <link href="{$baseMediaURL}/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<xsl:call-template name="user-css"/>
    <script>
      function changeMenu() {
        var selectBox = document.getElementById("panels-selector");
        var selectedValue = selectBox.options[selectBox.selectedIndex].value;
        $('#tabs_menu a[href="'+selectedValue+'"]').tab('show'); // Select tab by name
      }
</script>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="html-body">
    <div id="container" class="center-page">
      <article>
          <header id="page_header">
            <a href="index.html"><img src="{$baseMediaURL}images/logo.jpg" alt="logo Université Lille 1"/></a> 
            <h1>Catalogue des Formations</h1>
          </header>
        <xsl:call-template name="user-ariane"/>    
        <div class="row">
          <xsl:call-template name="user-col_gauche"/>
          <xsl:call-template name="user-col_droite"/>
        </div>
      </article>
    </div>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="html-body-cms">
        
        <div class="row">
          <xsl:call-template name="user-col_gauche"/>
          <xsl:call-template name="user-col_droite"/>
        </div>
        <xsl:call-template name="user-stat-cms"/>

  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="centre-accessibilite">
        <div id="centre-accessibilite" class="sr-only">
      <div id="zone-accessibilite">
        <ul>
          <li><a href="#tabs_menu">Aller au menu</a> |<xsl:text> </xsl:text></li>
        	<li><a href="#content" accesskey="s">Aller au contenu</a> |<xsl:text> </xsl:text>;</li>
        	<li><a href="#recherche">Aller à la recherche</a> |<xsl:text> </xsl:text></li>
          <li><a href="http://www.univ-lille1.fr/Accueil/Accessibilite" accesskey="0">Accessibilité</a></li>
        </ul>
      </div>
    </div>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" name="acces">
    <nav id="acces">
      <form action="http://www.univ-lille1.fr/Accueil/redirection" method="get" class="hidden-xs">
        <div>
          <div id="englobe_acces">
            <p>
              <label for="acces_direct">Accéder aux autres sites de l'Université Lille 1:</label>
            </p>
            <select name="lien" id="acces_direct">
              <xsl:apply-templates select="$redirections"/>
            </select>
            <input type="submit" title="Accéder au site" value="ok"/>
          </div>
        </div>
      </form>
      <!-- ce lien apparait quand < 767px de large pour remplacer le SELECT --> 
      <a href="http://www.univ-lille1.fr" title="Université lille 1" class="btn btn-default btn-xs pull-right visible-xs" id="goto_lille1"><span class="glyphicon glyphicon-globe"><xsl:text> </xsl:text></span><xsl:text> </xsl:text>Université Lille 1</a> 
    </nav>
  </xsl:template><!--FIN INCLUDE OF '../lille1-1.0/xhtml.xsl'-->
	<!--INCLUDE OF '../commun/composants/commun/infoBlock.cdmfr.xsl'--><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="subBlock[string-length(@userDefined)&gt;0]"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="subBlock[string-length(@userDefined)=0]"><p><xsl:apply-templates/></p></xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="br"><br/></xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="list[@listType='bulleted' or @listType='' or not(@listType)]"><ul><xsl:apply-templates/></ul></xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="list[@listType='numbered']"><ol><xsl:apply-templates/></ol></xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="listItem"><li><xsl:apply-templates/></li></xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="strong|emphasis|bold"><b><xsl:apply-templates/></b></xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="italic"><i><xsl:apply-templates/></i></xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="webLink"><a href="{href}"><xsl:value-of select="linkName"/></a></xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="email">
		<xsl:if test="contains(.,'@')">
			<p><a href="mailto:{.}"><xsl:value-of select="."/></a></p>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="searchword"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="header"><h3><xsl:value-of select="."/></h3></xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="infoBlock">
		<xsl:apply-templates select="node()"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="text">
		<xsl:value-of select="."/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="italic">
		<i>
			<xsl:apply-templates select="node()"/>
		</i>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="bold">
		<b>
			<xsl:apply-templates select="node()"/>
		</b>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="list">
		<ul>
			<xsl:apply-templates select="listItem"/>
			<!-- <xsl:apply-templates select="node()"/> -->
		</ul>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="listItem">
		<li>
			<xsl:apply-templates select="node()"/>
		</li>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="picture">
		<xsl:if test="contains(normalize-space(.),'http')"> <!-- il faudrait controler avec majuscule -->
			<xsl:call-template name="afficheImage">
				<xsl:with-param name="imageURL" select="."/>
			</xsl:call-template>
		</xsl:if>
		<!-- <xsl:apply-templates select="node()"/> -->
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="afficheImage">
		<xsl:param name="imageURL"/>
		<xsl:if test="not($imageURL='')">
			<img>
				<xsl:attribute name="src">
					<xsl:value-of select="normalize-space($imageURL)"/>
				</xsl:attribute>
			</img>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="webLink">
		<xsl:param name="lienExterieur" select="1"/>
		<xsl:param name="menu"/>
		<xsl:variable name="http" select="normalize-space(./href)"/>
		<!-- modification pour gerer la balise picture/webLink/href -->
		<xsl:choose>
			<xsl:when test="../picture">
				<!-- on ne traite pas le webLink comme l'URL de l'image à afficher -->
				<!-- <xsl:if test="not($http='')">
					<xsl:call-template name="afficheImage">
						<xsl:with-param name="imageURL" select="$http"/>
					</xsl:call-template>
				</xsl:if>
				-->
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="not($http='') and ($menu = 1)">
						<p class="aURL">
							<xsl:call-template name="liensuRL">
								<xsl:with-param name="lienExterieur" select="$lienExterieur"/>
							</xsl:call-template>
						</p>
					</xsl:when>
					<xsl:when test="not($http='')">
						<xsl:call-template name="liensuRL">
							<xsl:with-param name="lienExterieur" select="$lienExterieur"/>
						</xsl:call-template>
					</xsl:when>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="liensuRL">
		<xsl:param name="lienExterieur"/>
		<xsl:variable name="linkName" select="normalize-space(./linkName)"/>
		<xsl:variable name="intitule">
			<xsl:choose>
				<xsl:when test="not($linkName = '')">
					<xsl:value-of select="$linkName"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:choose>
						<xsl:when test="./@role='homepage'">
							<xsl:value-of select="$variablesExt/variables/navigationVar/link[@type='17']/navigationItem/value[@lang=$_lang]"/>
						</xsl:when>
						<xsl:when test="./@role='learningObjectMetadata'">
							<xsl:value-of select="$variablesExt/variables/navigationVar/link[@type='15']/navigationItem/value[@lang=$_lang]"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="./href"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<a accesskey=" ">
			<xsl:attribute name="title"><xsl:value-of select="$variablesExt/variables/navigationVar/link[@type='0']/linkTitle/value[@lang=$_lang]"/><xsl:text> </xsl:text><xsl:value-of select="$intitule"/></xsl:attribute>
			<xsl:if test="$lienExterieur = 1">
				<xsl:attribute name="onclick">return nleFenetre('<xsl:value-of select="normalize-space(./href)"/>')</xsl:attribute>
			</xsl:if>
			<xsl:attribute name="href"><xsl:apply-templates select="href"/></xsl:attribute>
			<xsl:value-of select="$intitule"/>
		</a>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="href">
		<xsl:value-of select="normalize-space(.)"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="linkName">
		<xsl:value-of select="normalize-space(.)"/>
	</xsl:template><!--FIN INCLUDE OF '../commun/composants/commun/infoBlock.cdmfr.xsl'-->
	<!--INCLUDE OF '../commun/composants/program.cdmfr.xsl'--><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramID">
		<xsl:apply-templates select="key('objet',$idP)/programID"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramPpalTeachingLanguage">
		<xsl:if test="key('objet',$idP)/ppalTeachingLanguage/@teachingLang"><li><xsl:call-template name="tCommunGestionLibelle"><xsl:with-param name="pIdent" select="'libTeachingLanguage'"/></xsl:call-template><xsl:value-of select="key('objet',$idP)/ppalTeachingLanguage"/></li></xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramWebLink">
		<xsl:apply-templates select="key('objet',$idP)/webLink" mode="enSavoirPlus"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" match="programDescription/globalVolume">
		<!--div><xsl:value-of select="@teachingtype"/> : <xsl:value-of select="text()"/></div-->
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramWebLinkHref">
		<xsl:apply-templates select="key('objet',$idP)/webLink/href"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramName">
		<xsl:apply-templates select="key('objet',$idP)/programName/text"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramCode">
		<xsl:param name="pCodeSet"/>
		<!-- Valeurs possibles: sectDGESIP sectDiscipSISE codeCNIS-NSF cite97 régimeInscriptionSise ERASMUS FAP userDefined -->
		<xsl:if test="not($pCodeSet='')">
			<xsl:apply-templates select="key('objet',$idP)/programCode[@codeSet=$pCodeSet]"/>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramCodeLibelDomain">
    <xsl:variable name="code" select="key('objet',$idP)/programCode/codeDomain"/>
    <xsl:value-of select="/CDM/properties/infoBlock/extension/uniform/constantes/domaineDiplome[@type='domaine']/data[@id=$code]/value[@lang=$_lang]"/>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramCodeLibelDomainAsListe">
		<li>
      <xsl:call-template name="get-lib"><xsl:with-param name="lib">libDomainePrincipal</xsl:with-param></xsl:call-template>
			<xsl:choose>
				<xsl:when test="count(key('objet',$idP)/programCode/codeDomain)&gt;1">
					<ul>
						<xsl:for-each select="key('objet',$idP)/programCode/codeDomain">
              <xsl:variable name="code" select="."/>
							<li><xsl:value-of select="/CDM/properties/infoBlock/extension/uniform/constantes/domaineDiplome[@type='domaine']/data[@id=$code]/value[@lang=$_lang]"/></li>
						</xsl:for-each>
					</ul>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="tProgramProgramCodeLibelDomain"/>
				</xsl:otherwise>
			</xsl:choose>
		</li>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramCodeLibelDomainAsString">
			<xsl:choose>
				<xsl:when test="count(key('objet',$idP)/programCode/codeDomain)&gt;1">
						<xsl:for-each select="key('objet',$idP)/programCode/codeDomain">
              <xsl:variable name="code" select="."/>
							<xsl:value-of select="/CDM/properties/infoBlock/extension/uniform/constantes/domaineDiplome[@type='domaine']/data[@id=$code]/value[@lang=$_lang]"/><xsl:if test="../following-sibling::programCode/codeDomain"> ; </xsl:if>
						</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="tProgramProgramCodeLibelDomain"/>
				</xsl:otherwise>
			</xsl:choose>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" match="programDescription/researchLink">
		<h3><xsl:call-template name="tCommunGestionLibelle"><xsl:with-param name="pIdent" select="'libLiaisonRecherche'"/></xsl:call-template></h3>
		<xsl:if test="relatedOrgUnit">
			<ul>
				<xsl:apply-templates select="relatedOrgUnit"/>
			</ul>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" match="programDescription/researchLink/relatedOrgUnit">
		<li><xsl:value-of select="relationType"/> : <xsl:value-of select="key('objet',refOrgUnit/@ref)/orgUnitName"/></li>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramDescriptionInfoBlock">
		<xsl:apply-templates select="key('objet',$idP)/programDescription/infoBlock"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramDescription">
		<xsl:apply-templates select="key('objet',$idP)/programDescription"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramFormOfTeaching">
		<xsl:apply-templates select="key('objet',$idP)/formOfTeaching"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramDescriptionTeachingTeamInfoBlock">
		<xsl:apply-templates select="key('objet',$idP)/programDescription/teachingTeam/infoBlock"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramDescriptionTeachingTeamGlobalVolume">
		<xsl:param name="pTeachingTypeSet"/>
		<!-- Valeurs possibles: CM, TD, TP, trainingWeeks, professTraining, homeWork, all -->
		<xsl:if test="not($pTeachingTypeSet='')">
			<xsl:apply-templates select="key('objet',$idP)/programDescription/teachingTeam/globalVolume[@teachingtype=$pTeachingTypeSet]"/>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramDescriptionNature">
		<xsl:apply-templates select="key('objet',$idP)/programDescription/@nature"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramQualificationQualificationDescription">
		<xsl:apply-templates select="key('objet',$idP)/qualification/qualificationDescription/infoBlock"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramQualificationStudyQualificationInfoBlock">
		<xsl:apply-templates select="key('objet',$idP)/qualification/studyQualification"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramQualificationProfession">
		<xsl:apply-templates select="key('objet',$idP)/qualification/profession/infoBlock"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramLearningObjectives">
		<xsl:apply-templates select="key('objet',$idP)/learningObjectives"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramQualificationCredits">
		<xsl:value-of select="key('objet',$idP)/qualification/credits/@ECTScredits"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramQualificationDegree">
		<xsl:value-of select="key('objet',$idP)/qualification/degree/@degree"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramQualificationDegreeWithText">
		<xsl:value-of select="key('objet',$_oid)/qualification/degree/@degree"/> [<xsl:apply-templates select="key('objet',$_oid)/qualification/degree/text() | key('objet',$_oid)/qualification/degree/*"/>]
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" match="qualification/degree/text()" name="tProgramQualificationDegreeInfoBlockType">
		<xsl:value-of select="key('objet',$_oid)/qualification/degree"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramQualificationProfessionRomeData">
		<xsl:if test="count(key('objet',$idP)/qualification/profession/romeData/text/node()) &gt;0 ">
			<ul>
				<xsl:for-each select="key('objet',$idP)/qualification/profession/romeData/text">
					<li>
						<xsl:value-of select="."/>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramQualificationProfessionRomeDataPdf">
		<fo:list-block>	
			<xsl:for-each select="key('objet',$idP)/qualification/profession/romeData/text">
				<fo:list-item>
					<fo:list-item-label><fo:block>•</fo:block></fo:list-item-label>
					<fo:list-item-body start-indent="4pt">
						<fo:block>
							<xsl:value-of select="."/>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>		
			</xsl:for-each>
		</fo:list-block>	
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramStudyAbroad">
		<xsl:apply-templates select="key('objet',$idP)/studyAbroad"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramStudyAbroadPdf">
		<fo:list-block>
			<xsl:for-each select="key('objet',$idP)/studyAbroad">
				<fo:list-item>
					<fo:list-item-label><fo:block>•</fo:block></fo:list-item-label>
					<fo:list-item-body start-indent="4pt">
						<fo:block>
							<xsl:apply-templates select="node()"/>
						</fo:block>
					</fo:list-item-body>
				</fo:list-item>	
			</xsl:for-each>
		</fo:list-block>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramIntroduction">
		<xsl:apply-templates select="key('objet',$idP)/programIntroduction"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramUniversalAdjustment">
		<xsl:apply-templates select="key('objet',$idP)/universalAdjustment"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramAdmissionInfoAdmissionDescription">
		<xsl:apply-templates select="key('objet',$idP)/admissionInfo/admissionDescription"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramAdmissionInfoStudentPlaces">
		<xsl:if test="key('objet',$idP)/admissionInfo/studentPlaces/@places"><li><xsl:call-template name="tCommunGestionLibelle"><xsl:with-param name="pIdent" select="'libNbPlaces'"/></xsl:call-template><xsl:value-of select="key('objet',$idP)/admissionInfo/studentPlaces/@places"/></li></xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramAdmissionInfoRegistrationStartDate">
		<xsl:if test="key('objet',$idP)/admissionInfo/registrationStart/@date"><li><xsl:call-template name="tCommunGestionLibelle"><xsl:with-param name="pIdent" select="'libDateDebutInscription'"/></xsl:call-template><xsl:value-of select="key('objet',$idP)/admissionInfo/registrationStart/@date"/></li></xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramAdmissionInforegistrationDeadlineDate">
		<xsl:if test="key('objet',$idP)/admissionInfo/registrationDeadline/@date">
			<li>
				<xsl:call-template name="tCommunGestionLibelle"><xsl:with-param name="pIdent" select="'libDateLimiteInscription'"/></xsl:call-template>
				<xsl:value-of select="key('objet',$idP)/admissionInfo/registrationDeadline/@date"/>
			</li>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramAdmissionInfostudentStatus">
		<xsl:if test="key('objet',$idP)/admissionInfo/studentStatus">
			<li><xsl:apply-templates select="key('objet',$idP)/admissionInfo/studentStatus"/></li>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramAipPossibleFollowUp">
		<xsl:apply-templates select="key('objet',$idP)/aip/possibleFollowUp"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramAipSuccessSupport">
		<xsl:apply-templates select="key('objet',$idP)/aip/successSupport"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramAipLearningPathStrategySupport">
		<xsl:apply-templates select="key('objet',$idP)/aip/learningPathStrategySupport"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramAipProfessTrainingSupport">
		<xsl:apply-templates select="key('objet',$idP)/aip/professTrainingSupport"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramTrainingStrategy">
		<xsl:apply-templates select="key('objet',$idP)/habilitation/partnership/training/trainingStrategy"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramExpensesInfoBlockType">
		<xsl:call-template name="tExpensesInfoBlockType">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramExpensesPrice">
		<xsl:call-template name="tExpensesPrice">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramFormalPrerequisites">
		<xsl:apply-templates select="key('objet',$idP)/formalPrerequisites"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramRecommendedPrerequisites">
		<xsl:apply-templates select="key('objet',$idP)/recommendedPrerequisites"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramStructureInfoBlock">
		<xsl:apply-templates select="key('objet',$idP)/programStructure/infoBlock"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramStructure">
		<xsl:if test="not(key('objet',$idP)/programStructure/infoBlock/header)"><h3>Programme</h3></xsl:if>
		<xsl:apply-templates select="key('objet',$idP)/programStructure/infoBlock"/>
		<xsl:if test="key('objet',$idP)/programStructure/refProgram">
			<ul>
				<xsl:apply-templates select="key('objet',$idP)/programStructure/refProgram"/>
			</ul>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramStructureRefProgram">
		<xsl:if test="count(key('objet',$idP)/programStructure/refProgram)&gt;0">
			<ul>
				<xsl:apply-templates select="key('objet',$idP)/programStructure/refProgram"/>
			</ul>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramRefProgramProgramNameText">
		<xsl:param name="pRef"/>
		<xsl:value-of select="key('objet',@ref)/programName/text"/><xsl:text> </xsl:text>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramStructurePdf">
		<xsl:if test="count(key('objet',$idP)/programStructure/refProgram)&gt;0">
			<fo:list-block>
				<xsl:for-each select="key('objet',$idP)/programStructure/refProgram">					
					<fo:list-item>
						<fo:list-item-label><fo:block>•</fo:block></fo:list-item-label>
						<fo:list-item-body start-indent="4pt">
							<fo:block>
								<xsl:value-of select="key('objet',@ref)/programName/text"/>
							</fo:block>
						</fo:list-item-body>
					</fo:list-item>		
				</xsl:for-each>				
			</fo:list-block>
		</xsl:if>
		<xsl:if test="count(key('objet',$idP)/programStructure/refCourse)&gt;0">
			<fo:list-block>
				<xsl:for-each select="key('objet',$idP)/programStructure/refCourse">					
					<fo:list-item>
						<fo:list-item-label><fo:block>•</fo:block></fo:list-item-label>
						<fo:list-item-body start-indent="4pt">
							<fo:block>
								<xsl:value-of select="key('objet',@ref)/courseName"/>
							</fo:block>
						</fo:list-item-body>
					</fo:list-item>		
				</xsl:for-each>				
			</fo:list-block>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramRegulations">
		<xsl:apply-templates select="key('objet',$idP)/regulations"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramFormOfAssessment">
		<xsl:apply-templates select="key('objet',$idP)/formOfAssessment"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramLevel">
		<xsl:call-template name="tLevelLevel">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramLearningObjectivesInfoBlockType">
		<xsl:call-template name="tLearningObjectivesInfoBlockType">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramLevelCode">
		<xsl:value-of select="key('objet',$idP)/levelCode/@codeSet"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramLevelLevel">
		<xsl:call-template name="tLevelLevel">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramAdmissionInfoEctsRequiredValue">
		<xsl:value-of select="sum(key('objet',$idP)/admissionInfo/ectsRequired/@value)"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramProgramDuration">
		<xsl:if test="key('objet',$idP)/programDuration">
			<li><xsl:call-template name="tCommunGestionLibelle"><xsl:with-param name="pIdent" select="'libDureeFormation'"/></xsl:call-template><xsl:apply-templates select="key('objet',$idP)/programDuration"/></li>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramTargetGroup">
		<xsl:if test="key('objet',$idP)/targetGroup"><li><xsl:apply-templates select="key('objet',$idP)/targetGroup"/></li></xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramTargetGroupPdf">
		<xsl:if test="key('objet',$idP)/targetGroup"><xsl:apply-templates select="key('objet',$idP)/targetGroup"/></xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" match="teachingPlace">
		<li><xsl:apply-templates/></li>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" match="teachingPlace/refOrgUnit">
		<div>
			<div><xsl:apply-templates select="key('objet', @ref)/orgUnitName"/></div>
			<div><xsl:apply-templates select="key('objet', @ref)/contacts/contactData/adr"/></div>
		</div>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" match="teachingPlace/adr">
		<xsl:if test="not(key('objet', ../refProgram/@ref)/contacts/contactData/adr)">
			<xsl:apply-templates/>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramTeachingPlaceInfoBlock">
		<xsl:apply-templates select="key('objet',$idP)/teachingPlace/infoBlock"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramTeachingPlace">
		<xsl:if test="key('objet',$idP)/teachingPlace">
			<li>
				<xsl:call-template name="tCommunGestionLibelle"><xsl:with-param name="pIdent" select="'libAdresseEnseignement'"/></xsl:call-template>
				<ul>
					<xsl:apply-templates select="key('objet',$idP)/teachingPlace"/>
				</ul>
			</li>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramTeachingPlaceAdrExtadr">
		<xsl:value-of select="key('objet',$idP)/teachingPlace/adr/extadr"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramTeachingPlaceAdrStreet">
		<xsl:value-of select="key('objet',$idP)/teachingPlace/adr/street"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramTeachingPlaceAdrPcode">
		<xsl:value-of select="key('objet',$idP)/teachingPlace/adr/pcode"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramTeachingPlaceAdrLocality">
		<xsl:value-of select="key('objet',$idP)/teachingPlace/adr/locality"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tProgramSearchword">
		<xsl:if test="count(key('objet',$idP)/searchword) &gt; 0">
			<ul>
				<li><xsl:apply-templates select="key('objet',$idP)/searchword"/></li>
			</ul>
		</xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tResumeFormation">
		<h3><xsl:call-template name="tCommunGestionLibelle"><xsl:with-param name="pIdent" select="'libResumeFormation'"/></xsl:call-template></h3>
		<ul>
			<li>
				<xsl:call-template name="tCommunGestionLibelle"><xsl:with-param name="pIdent" select="'libAdresseEnseignement'"/></xsl:call-template>
				<xsl:apply-templates select="key('objet',$idP)/teachingPlace/infoBlock"/>
			</li>
		</ul>
	</xsl:template><!--FIN INCLUDE OF '../commun/composants/program.cdmfr.xsl'-->
	<!--INCLUDE OF '../commun/composants/person.cdmfr.xsl'--><xsl:variable xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="vNode" select="key('objet',$idP)"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonNameGiven">
		<xsl:param name="pRefPerson"/>
		<xsl:choose>
			<xsl:when test="not($pRefPerson='')">
				<xsl:value-of select="$pRefPerson/name/given"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$vNode/name/given"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonNameFamily">
		<xsl:param name="pRefPerson"/>
		<xsl:choose>
			<xsl:when test="not($pRefPerson='')">
				<xsl:value-of select="$pRefPerson/name/family"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$vNode/name/family"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonTitle">
		<xsl:value-of select="$vNode/title/text[@language=$_lang or not(@language)]"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonRole">
		<xsl:param name="pRefPerson"/>
		<xsl:choose>
			<xsl:when test="not($pRefPerson='')">
				<xsl:value-of select="$pRefPerson/role/text[@language=$_lang or not(@language)]"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$vNode/role/text[@language=$_lang or not(@language)]"/>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonContactDataAdrPobox">
		<xsl:value-of select="$vNode/contactData/adr/pobox"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonContactDataAdrExtadr">
		<xsl:value-of select="$vNode/contactData/adr/extadr"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonContactDataAdrStreet">
		<xsl:value-of select="$vNode/contactData/adr/street"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonContactDataAdrLocality">
		<xsl:value-of select="$vNode/contactData/adr/locality"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonContactDataAdrPcode">
		<xsl:value-of select="$vNode/contactData/adr/pcode"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonContactDataAdrCountry">
		<xsl:value-of select="$vNode/contactData/adr/country"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonContactDataVisitorHour">
		<xsl:value-of select="$vNode/contactData/visitorHour/text[@language=$_lang or not(@language)]"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonContactDataTelephone">
		<xsl:param name="pRefPerson"/>
		<xsl:choose>
			<xsl:when test="not($pRefPerson='')">
				<xsl:value-of select="$pRefPerson/contactData/telephone"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$vNode/contactData/telephone"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonContactDataFax">
		<xsl:value-of select="$vNode/contactData/fax"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonContactDataEmail">
		<xsl:param name="pRefPerson"/>
		<xsl:choose>
			<xsl:when test="not($pRefPerson='')">
				<xsl:apply-templates select="$pRefPerson/contactData/email"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="$vNode/contactData/email"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonActivitiestTeachingMajor">
		<xsl:value-of select="$vNode/activities/teachingMajor/text[@language=$_lang or not(@language)]"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tPersonActivitiestOtherActivities">
		<xsl:value-of select="$vNode/activities/teachingMajor/text[@language=$_lang or not(@language)]"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tContactsRefPersonGivenName">
		<!--<xsl:value-of select="key('objet',$vNode/contacts/refPerson/@ref)/name/given"/>-->
		<xsl:param name="pRef"/>
		<xsl:value-of select="/CDM/person[@ident=$pRef]/name/given"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tContactsRefPersonNameFamily">
		<xsl:param name="pRef"/>
		<xsl:value-of select="/CDM/person[@ident=$pRef]/name/family"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tContactsRefPersonNameRole">
		<xsl:param name="pRef"/>
    <xsl:call-template name="get-lib"><xsl:with-param name="lib"><xsl:value-of select="key('objet',$idP)/contacts/refPerson[@ref=$pRef]/@role"/></xsl:with-param></xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tContactsRefPersonNameContactDataTelephone">
		<xsl:param name="pRef"/>
		<xsl:value-of select="/CDM/person[@ident=$pRef]/contactData/telephone"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tContactsRefPersonNameContactDataEmail">
		<xsl:param name="pRef"/>
		<xsl:apply-templates select="/CDM/person[@ident=$pRef]/contactData/email"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tContactsRefPerson">
		<xsl:param name="pObjet"/>
		<!-- on teste si la référence existe -->
		<xsl:choose>
			<xsl:when test="key('objet',$vNode/contacts/refPerson/@ref)">
				<!-- la personne existe -->
				<xsl:choose>
					<xsl:when test="$pObjet='PersonNameGiven'">
						<xsl:call-template name="tPersonNameGiven">
							<xsl:with-param name="pRefPerson" select="key('objet',$vNode/contacts/refPerson/@ref)"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="$pObjet='PersonNameFamily'">
						<xsl:call-template name="tPersonNameFamily">
							<xsl:with-param name="pRefPerson" select="key('objet',$vNode/contacts/refPerson/@ref)"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="$pObjet='PersonRole'">
            <xsl:call-template name="get-lib"><xsl:with-param name="lib"><xsl:value-of select="$vNode/contacts/refPerson/@role"/></xsl:with-param></xsl:call-template>
						<!-- 
						<xsl:call-template name="tPersonRole">
							<xsl:with-param name="pRefPerson" select="key('objet',$vNode/contacts/refPerson/@ref)"/>
						</xsl:call-template>
						-->
					</xsl:when>
					<xsl:when test="$pObjet='PersonContactDataTelephone'">
						<xsl:call-template name="tPersonContactDataTelephone">
							<xsl:with-param name="pRefPerson" select="key('objet',$vNode/contacts/refPerson/@ref)"/>
						</xsl:call-template>
					</xsl:when>   
					<xsl:when test="$pObjet='PersonContactDataEmail'">
						<xsl:call-template name="tPersonContactDataEmail">
							<xsl:with-param name="pRefPerson" select="key('objet',$vNode/contacts/refPerson/@ref)"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<!-- ON NE FAIT RIEN -->
					</xsl:otherwise>
				</xsl:choose>

			</xsl:when>
			<xsl:otherwise>
				<!-- la personne n existe pas : ON NE FAIT RIEN -->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" name="tContactsRefPerson2">
		<xsl:apply-templates select="key('objet',$idP)/contacts/refPerson"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:fo="http://www.w3.org/1999/XSL/Format" match="affiliation">
		<xsl:apply-templates select="refOrgUnit"/>
		<xsl:call-template name="tSectionCNU">
			<xsl:with-param name="pCode" select="sectCNU/@code"/>
			<xsl:with-param name="pName" select="sectCNU/@name"/>
		</xsl:call-template>
	</xsl:template><!--FIN INCLUDE OF '../commun/composants/person.cdmfr.xsl'-->
	<!--INCLUDE OF '../commun/composants/course.cdmfr.xsl'--><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseCourseID">
		<xsl:value-of select="key('objet',$idP)/courseID"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseCourseName">
		<xsl:value-of select="key('objet',$idP)/courseName"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseCourseCode">
		<xsl:value-of select="key('objet',$idP)/courseCode/@codeSet"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseCourseDescription">
		<xsl:apply-templates select="key('objet',$idP)/courseDescription"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseLevelInfoBlockType">
		<xsl:call-template name="tLevelInfoBlockType">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseLevelLanguageCECRLLevel">
		<xsl:call-template name="tLevelLanguageCECRLLevel">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseCreditsECTScredits">
		<xsl:call-template name="tCreditsECTScredits">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseCreditsInfoBlockType">
		<xsl:call-template name="tCreditsInfoBlockType">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseCreditsGlobalVolume">
		<xsl:call-template name="tCreditsGlobalVolume">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseLearningObjectivesInfoBlockType">
		<xsl:call-template name="tLearningObjectivesInfoBlockType">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseAdmissionInfoAdmissionDescription">
		<xsl:call-template name="tAdmissionInfoAdmissionDescription">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseRecommendedPrerequisites">
		<xsl:call-template name="tRecommendedPrerequisites">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseFormalPrerequisites">
		<xsl:call-template name="tFormalPrerequisites">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseFormOfTeaching">
		<xsl:call-template name="tFormOfTeaching">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseFormOfAssessment">
		<xsl:call-template name="tFormOfAssessment">
			<xsl:with-param name="pNode" select="key('objet',$idP)"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseInstructionLanguageTeachingLang">
		<xsl:choose>
			<xsl:when test="key('objet',$idP)/instructionLanguage/@teachingLang = 'en'">anglais
				<!--img src="{$baseMediaURL}/images/en.gif" alt="English" /-->
			</xsl:when>
			<xsl:when test="key('objet',$idP)/instructionLanguage/@teachingLang = 'es'">espagnol
				<!--img src="{$baseMediaURL}/images/es.gif" alt="Española" /-->
			</xsl:when>
			<xsl:otherwise>français
				<!--img src="{$baseMediaURL}/images/fr.gif" alt="Français" /-->
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseInstructionLanguage">
		<xsl:value-of select="key('objet',$idP)/instructionLanguage"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tCourseSyllabus">
		<xsl:apply-templates select="key('objet',$idP)/syllabus"/>
	</xsl:template><!--FIN INCLUDE OF '../commun/composants/course.cdmfr.xsl'-->
	<!--INCLUDE OF '../commun/composants/orgUnit.cdmfr.xsl'--><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="refOrgUnit">
		<li><xsl:call-template name="tCommunGestionLibelle">
				<xsl:with-param name="pIdent" select="'libEtablissementDeReference'"/>
			</xsl:call-template><xsl:call-template name="tOrgUnitName"/></li>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" match="refOrgUnit[not(@ref)]"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitName">
		<xsl:value-of select="/CDM/orgUnit[1]/orgUnitName"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitSecondName">
		<xsl:param name="pSecondNameNature" select="'patronyme'"/>
		<xsl:value-of select="/CDM/orgUnit[1]/orgUnitSecondName[@secondNameNature=$pSecondNameNature]/secondName/text"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitCode">
		<xsl:param name="pCodeSet" select="'codeUAI'"/>
		<xsl:value-of select="/CDM/orgUnit[1]/orgUnitCode[@codeSet=$pCodeSet]"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitKind">
		<xsl:apply-templates select="/CDM/orgUnit[1]/orgUnitKind"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitWebLinkHref">
		<xsl:apply-templates select="/CDM/orgUnit[1]/webLink/href"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitDescription">
		<xsl:apply-templates select="/CDM/orgUnit[1]/orgUnitDescription"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitIntroduction">
		<xsl:apply-templates select="/CDM/orgUnit[1]/orgUnitIntroduction"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitAdmissionInfo">
		<xsl:call-template name="tOrgUnitAdmissionInfoAdmissionDescription"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitAdmissionInfoAdmissionDescription">
		<xsl:apply-templates select="/CDM/orgUnit[1]/admissionInfo/admissionDescription"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitRegistrationDeadline">
		<xsl:param name="pLibelleDateDeadline"/>
		<xsl:if test="not($pLibelleDateDeadline='')">
			<xsl:value-of select="$pLibelleDateDeadline"/> <xsl:value-of select="/CDM/orgUnit[1]/admissionInfo/registrationDeadline/@date"/>
		</xsl:if>
		<xsl:apply-templates select="/CDM/orgUnit[1]/admissionInfo/registrationDeadline"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitCancelDeadline">
		<xsl:param name="pLibelleDateCancel"/>
		<xsl:if test="not($pLibelleDateCancel='')">
			<xsl:value-of select="$pLibelleDateCancel"/> <xsl:value-of select="/CDM/orgUnit[1]/admissionInfo/cancelDeadline/@date"/>
		</xsl:if>
		<xsl:apply-templates select="/CDM/orgUnit[1]/admissionInfo/cancelDeadline"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitRegulations">
		<xsl:apply-templates select="/CDM/orgUnit[1]/regulations[@blockLang=$_lang or not(@blockLang)]"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitExpensesInfoBlockType">
		<xsl:call-template name="tExpensesInfoBlockType">
			<xsl:with-param name="pNode" select="/CDM/orgUnit[1]"/>
		</xsl:call-template>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitStudentFacilities">
		<xsl:apply-templates select="/CDM/orgUnit[1]/studentFacilities[@blockLang=$_lang or not(@blockLang)]"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitUniversalAdjustment">
		<xsl:apply-templates select="/CDM/orgUnit[1]/universalAdjustment[@blockLang=$_lang or not(@blockLang)]"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitQualiteSignataireDiplome">
		<xsl:apply-templates select="/CDM/orgUnit[1]/infoBlock"/>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tOrgUnitContactData">
		<xsl:apply-templates select="/CDM/orgUnit[1]/contacts/contactData"/>
	</xsl:template><!--FIN INCLUDE OF '../commun/composants/orgUnit.cdmfr.xsl'-->
	<!--INCLUDE OF '../commun/composants/properties.cdmfr.xsl'--><xsl:variable xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="pNode" select="/CDM/properties"/><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tPropertiesDateTimeDate">
		<xsl:value-of select="$pNode/datetime/@date"/>		
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tPropertiesDateTimeTransformDate">
		<xsl:value-of select="substring($pNode/datetime/@date,9,2)"/>
		<xsl:text>-</xsl:text>
		<xsl:value-of select="substring($pNode/datetime/@date,6,2)"/>
		<xsl:text>-</xsl:text>
		<xsl:value-of select="substring($pNode/datetime/@date,1,4)"/>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tPropertiesDateTimeTime">
		<xsl:value-of select="$pNode/datetime/@time"/>
	</xsl:template><xsl:template xmlns:cdmfr="http://cdm-fr.fr/2011/CDM-frSchema" xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" name="tPropertiesDateTimeTransformTime">
		<xsl:value-of select="substring($pNode/datetime/@time,1,2)"/>
		<xsl:text>H</xsl:text>
		<xsl:value-of select="substring($pNode/datetime/@time,4,2)"/>
	</xsl:template><!--FIN INCLUDE OF '../commun/composants/properties.cdmfr.xsl'-->

	<!--INCLUDE OF '../lille1-1.0/onglets/presentation.xsl'--><xsl:output xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" method="xml" indent="yes" media-type="text/html" encoding="UTF-8" omit-xml-declaration="yes" xml:lang="fr" xml:space="preserve"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" name="user-presentation">
    <hr class="visible-print"/>
    <xsl:if test="string-length(key('objet',$mention)/programDescription/*//text())&gt;0"><h3>Objectifs</h3></xsl:if>
    <xsl:apply-templates select="key('objet',$mention)/programDescription" mode="screen"/>
    <xsl:for-each select="key('objet',$mention)/programStructure//refProgram[@role='parcours']">
      <div class="cb-{@ref} cb"><xsl:if test="not(preceding-sibling::refProgram[@role='parcours'])"><xsl:attribute name="style">display:block;</xsl:attribute></xsl:if>
        <xsl:if test="string-length(key('objet',@ref)/programDescription/*//text())&gt;0"><h3>Objectifs</h3></xsl:if>
        <xsl:text> </xsl:text>
        <xsl:apply-templates select="key('objet',@ref)/programDescription" mode="screen"/>
      </div>
    </xsl:for-each>
		<xsl:if test="string-length(key('objet',$mention)/learningObjectives/*//text())&gt;0"><h3>Spécificités</h3></xsl:if>
    <xsl:apply-templates select="key('objet',$mention)/learningObjectives" mode="screen"/>
    <xsl:for-each select="key('objet',$mention)/programStructure//refProgram[@role='parcours']">
       
      <div class="cb-{@ref} cb"><xsl:if test="not(preceding-sibling::refProgram[@role='parcours'])"><xsl:attribute name="style">display:block;</xsl:attribute></xsl:if>
        <xsl:if test="string-length(key('objet',$mention)/learningObjectives/*//text())=0 and string-length(key('objet',@ref)/learningObjectives/*//text())&gt;0"><h3>Spécificités</h3></xsl:if>
        <xsl:text> </xsl:text>
        <xsl:apply-templates select="key('objet',@ref)/learningObjectives" mode="screen"/>
      </div>
    </xsl:for-each>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="programDescription//header"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="programDescription//subBlock/header"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="learningObjectives//subBlock/header"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="qualification/qualificationDescription//subBlock/header"/><!--FIN INCLUDE OF '../lille1-1.0/onglets/presentation.xsl'-->
	<!--INCLUDE OF '../lille1-1.0/onglets/contact.xsl'--><xsl:output xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" method="xml" indent="yes" media-type="text/html" encoding="UTF-8" omit-xml-declaration="yes" xml:lang="fr" xml:space="preserve"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" name="user-contacts">
    <hr class="visible-print"/>
    <h3>Composantes</h3>
    <xsl:for-each select="key('objet',$mention)/programStructure//refProgram[@role='parcours']">
      <div class="cb-{@ref} cb"><xsl:text> </xsl:text>
        <ul>
          <xsl:for-each select="key('objet',$mention)/programCode[@codeSet='uniform.composante']">
            <xsl:variable name="code" select="."/>
            <xsl:apply-templates select="." mode="contacts"/>
          </xsl:for-each>
          <xsl:for-each select="key('objet',$mention)/programCode[@codeSet='uniform.composante'] | key('objet',@ref)/programCode[@codeSet='uniform.composante']">
            <xsl:variable name="code" select="."/>
            <xsl:if test="not(key('objet',$mention)/programCode[@codeSet='uniform.composante']=$code)"><xsl:apply-templates select="." mode="contacts"/></xsl:if>
          </xsl:for-each>
        </ul>
      </div>
    </xsl:for-each>
    <h3>Personnes à contacter</h3>
    <xsl:apply-templates select="key('objet',$mention)/contacts/refPerson" mode="contacts"/>
    <xsl:for-each select="key('objet',$mention)/programStructure//refProgram[@role='parcours']">
      <div class="cb-{@ref} cb"><xsl:text> </xsl:text>
        <xsl:if test="key('objet',@ref)/contacts/refPerson[@role='R'] and (not(key('objet',@ref)/contacts/refPerson[@role='R1']) and not(key('objet',@ref)/contacts/refPerson[@role='R2']) and not(key('objet',@ref)/contacts/refPerson[@role='R3']))">
          <h5>Responsable</h5>
          <xsl:apply-templates select="key('objet',@ref)/contacts/refPerson[@role='R']" mode="contacts"/>
        </xsl:if>
        <xsl:if test="key('objet',@ref)/contacts/refPerson[@role='S'] and (not(key('objet',@ref)/contacts/refPerson[@role='S1']) and not(key('objet',@ref)/contacts/refPerson[@role='S2']) and not(key('objet',@ref)/contacts/refPerson[@role='S3']))">
          <h5>Secrétariat</h5>
          <xsl:apply-templates select="key('objet',@ref)/contacts/refPerson[@role='S']" mode="contacts"/>
        </xsl:if>
        <xsl:if test="key('objet',@ref)/contacts/refPerson[@role='R1'] or key('objet',@ref)/contacts/refPerson[@role='S1']">
          <h4>Première année</h4>
          <xsl:if test="key('objet',@ref)/contacts/refPerson[@role='R1']">
            <h5>Responsable</h5>
            <xsl:apply-templates select="key('objet',@ref)/contacts/refPerson[@role='R1']" mode="contacts"/>
          </xsl:if>
          <xsl:if test="key('objet',@ref)/contacts/refPerson[@role='S1']">
            <h5>Secrétariat</h5>
            <xsl:apply-templates select="key('objet',@ref)/contacts/refPerson[@role='S1']" mode="contacts"/>
          </xsl:if>
        </xsl:if>
        <xsl:if test="key('objet',@ref)/contacts/refPerson[@role='R2'] or key('objet',@ref)/contacts/refPerson[@role='S2']">
          <h4>Deuxième année</h4>
          <xsl:if test="key('objet',@ref)/contacts/refPerson[@role='R2']">
            <h5>Responsable</h5>
            <xsl:apply-templates select="key('objet',@ref)/contacts/refPerson[@role='R2']" mode="contacts"/>
          </xsl:if>
          <xsl:if test="key('objet',@ref)/contacts/refPerson[@role='S2']">
            <h5>Secrétariat</h5>
            <xsl:apply-templates select="key('objet',@ref)/contacts/refPerson[@role='S2']" mode="contacts"/>
          </xsl:if>
        </xsl:if>
        <xsl:if test="key('objet',@ref)/contacts/refPerson[@role='R3'] or key('objet',@ref)/contacts/refPerson[@role='S3']">
          <h4>Troisième année</h4>
          <xsl:if test="key('objet',@ref)/contacts/refPerson[@role='R3']">
            <h5>Responsable</h5>
            <xsl:apply-templates select="key('objet',@ref)/contacts/refPerson[@role='R3']" mode="contacts"/>
          </xsl:if>
          <xsl:if test="key('objet',@ref)/contacts/refPerson[@role='S3']">
            <h5>Secrétariat</h5>
            <xsl:apply-templates select="key('objet',@ref)/contacts/refPerson[@role='S3']" mode="contacts"/>
          </xsl:if>
        </xsl:if>
      </div>
    </xsl:for-each>
 	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="programCode" mode="composante">
    <xsl:variable name="code" select="."/> 
     <xsl:if test="not(preceding-sibling::programCode[text()=$code])"><xsl:apply-templates select="." mode="contacts"/></xsl:if>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="refPerson" mode="contacts">
    <xsl:if test="key('objet',@ref)[1]/contactData/email">
      <address>
        <xsl:if test="key('objet',@ref)[1]/contactData/telephone/text()"><span class="glyphicon glyphicon-earphone"><xsl:text> </xsl:text></span><xsl:text> </xsl:text><xsl:value-of select="key('objet',@ref)[1]/contactData/telephone"/><br/></xsl:if>
        <span class="glyphicon glyphicon-envelope"><xsl:text> </xsl:text></span><xsl:text> </xsl:text><a href="mailto:{key('objet',@ref)[1]/contactData/email}"><xsl:value-of select="key('objet',@ref)[1]/contactData/email"/></a>
      </address>
    </xsl:if>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="programCode" mode="contacts">
    <xsl:variable name="ref" select="."/>
      <li><a href="{/CDM/orgUnit[1]/orgUnit[@ident=$ref or @id=$ref]/webLink/href}"><xsl:value-of select="/CDM/orgUnit[1]/orgUnit[@ident=$ref or @id=$ref]/orgUnitName/text"/></a></li>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="profession/subBlock//header"/><!--FIN INCLUDE OF '../lille1-1.0/onglets/contact.xsl'-->
	<!--INCLUDE OF '../lille1-1.0/onglets/poursuite.xsl'--><xsl:output xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" method="xml" indent="yes" media-type="text/html" encoding="UTF-8" omit-xml-declaration="yes" xml:lang="fr" xml:space="preserve"/><xsl:variable xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" name="nb-poursuite"><xsl:value-of select="count(key('objet',$idP)/qualification/studyQualification//text())+count(key('program',$idP)/ancestor-or-self::program/poursuites/refProgram)"/></xsl:variable><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" name="user-poursuite">
    <hr class="visible-print"/>
    <xsl:if test="string-length(key('objet',$mention)/qualification/profession/*//text())&gt;0"><h3>Poursuite d'études et insertion professionnelle</h3></xsl:if>
    <xsl:apply-templates select="key('objet',$mention)/qualification/profession" mode="screen"/>
    <xsl:for-each select="key('objet',$mention)/programStructure//refProgram[@role='parcours']">
      <div class="cb-{@ref} cb"><xsl:text> </xsl:text>
        <xsl:if test="string-length(key('objet',@ref)/qualification/profession/*//text())&gt;0"><h3>Poursuite d'études et insertion professionnelle</h3></xsl:if>
        <xsl:apply-templates select="key('objet',@ref)/qualification/profession" mode="screen"/>
      </div>
    </xsl:for-each>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="studyQualification/subBlock//header"/><!--FIN INCLUDE OF '../lille1-1.0/onglets/poursuite.xsl'-->
	<!--INCLUDE OF '../lille1-1.0/onglets/competences.xsl'--><xsl:output xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" method="xml" indent="yes" media-type="text/html" encoding="UTF-8" omit-xml-declaration="yes" xml:lang="fr" xml:space="preserve"/><xsl:variable xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" name="nb-competences"><xsl:value-of select="count(key('objet',$idP)/qualification/profession//text())"/></xsl:variable><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" name="user-competences">
    <hr class="visible-print"/>
		<h3>Les savoirs</h3>
    <xsl:apply-templates select="key('objet',$mention)/qualification/qualificationDescription" mode="screen"/>
    <xsl:for-each select="key('objet',$mention)/programStructure//refProgram[@role='parcours']">
      <div class="cb-{@ref} cb"><xsl:text> </xsl:text>
        <xsl:apply-templates select="key('objet',@ref)/qualification/qualificationDescription" mode="screen"/>
      </div>
    </xsl:for-each>
    	<h3>Les savoir-faire</h3>
    <xsl:apply-templates select="key('objet',$mention)/qualification/studyQualification" mode="screen"/>
    <xsl:for-each select="key('objet',$mention)/programStructure//refProgram[@role='parcours']">
      <div class="cb-{@ref} cb"><xsl:text> </xsl:text>
        <xsl:apply-templates select="key('objet',@ref)/qualification/studyQualification" mode="screen"/>
      </div>
    </xsl:for-each>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="profession/subBlock//header"/><!--FIN INCLUDE OF '../lille1-1.0/onglets/competences.xsl'-->
  <!--INCLUDE OF '../lille1-1.0/onglets/programme.xsl'--><xsl:output xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" method="xml" indent="yes" media-type="text/html" encoding="UTF-8" omit-xml-declaration="yes" xml:lang="fr" xml:space="preserve"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" name="user-programme">
    <hr class="visible-print"/>
    <h3>Tableau des semestres</h3>
    <!--p>Le tableau des semestres est en cours de réalisation. Ce n'est pas encore la version définitive ni pour ses données ni pour sa représentation.</p-->
    <xsl:apply-templates select="key('objet',$mention)/programStructure" mode="programme"/>
     <xsl:for-each select="key('objet',$mention)/programStructure/refProgram[@role='parcours']">
      <div class="cb-{@ref} cb"><xsl:text> </xsl:text>
        <xsl:apply-templates select="key('objet',@ref)/programStructure" mode="programme"/>
      </div>
    </xsl:for-each>
    <xsl:apply-templates select="key('objet',$mention)/programStructure" mode="programme-print"/>
    <xsl:for-each select="key('objet',$mention)/programStructure/refProgram[@role='parcours']">
      <div class="cb-{@ref} cb print"><xsl:text> </xsl:text>
        <xsl:apply-templates select="key('objet',@ref)/programStructure" mode="programme-print"/>
      </div>
    </xsl:for-each>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="programStructure" mode="programme">
    
    <xsl:if test="../subProgram"> 
        <table class="table table-responsive screen">
          <tr>
            <th scope="col">Semestre</th>
            <th scope="col">Unité d'Enseignement</th>
            <th scope="col">Crédits</th>
          </tr>
          <xsl:apply-templates select="refProgram[@role='semestre']" mode="semestre"/>
          <!--xsl:apply-templates select="refProgram[@role='parcours']" mode="semestre"/-->
        </table>
      
    </xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="programStructure" mode="programme-print">
    
    <xsl:if test="../subProgram"> 
     
        <xsl:apply-templates select="refProgram[@role='semestre']" mode="semestre-print"/>
    </xsl:if>
	</xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="refProgram" mode="semestre">
    <xsl:apply-templates select="key('objet',@ref)[1]/programStructure" mode="semestre"/>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="refProgram" mode="semestre-print">
    <ul class="visible-print"><xsl:apply-templates select="key('objet',@ref)[1]/programStructure" mode="semestre-print"/></ul>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="programStructure" mode="semestre">
    <xsl:if test="subBlock/refCourse">
      <tr>
        <th class="active" scope="row" rowspan="{count(subBlock[refCourse])+count(subBlock/refCourse)+1}"><xsl:value-of select="../programName/text"/></th>
        <td class="active"><xsl:text> </xsl:text></td>
        <td class="active"><xsl:text> </xsl:text></td>
      </tr>
    </xsl:if>
    <xsl:apply-templates select="subBlock[refCourse]" mode="semestre"/>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="programStructure" mode="semestre-print">
  <xsl:if test="subBlock/refCourse">
    <li>
    
      
      <xsl:value-of select="../programName/text"/>
    
      <ul><xsl:apply-templates select="subBlock[refCourse]" mode="semestre-print"/></ul>
    </li>
    </xsl:if>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="subBlock/refCourse" mode="semestre">
		<tr>
      <td><xsl:value-of select="key('objet', ./@ref)/courseName/text"/></td>
      <td><xsl:value-of select="key('objet', ./@ref)/credits/@ECTScredits"/></td>
    </tr>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="subBlock/refCourse" mode="semestre-print">
      <li><xsl:value-of select="key('objet', ./@ref)/courseName/text"/> (<xsl:value-of select="key('objet', ./@ref)/credits/@ECTScredits"/> ECTS)</li>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="subBlock" mode="semestre">
    <xsl:if test="refCourse">
      <tr>
        <td class="ue"><xsl:value-of select="header"/></td>
        <td class="ue"><xsl:text> </xsl:text></td>
      </tr>
    </xsl:if>
    <xsl:apply-templates select="refCourse" mode="semestre"/>
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="subBlock" mode="semestre-print">
    <xsl:if test="refCourse">
        <li><xsl:value-of select="header"/>
        <ul><xsl:apply-templates select="refCourse" mode="semestre-print"/></ul>
        </li>

      
    </xsl:if>
    
  </xsl:template><!--FIN INCLUDE OF '../lille1-1.0/onglets/programme.xsl'-->
	<!--INCLUDE OF '../lille1-1.0/onglets/admission.xsl'--><xsl:output xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" method="xml" indent="yes" media-type="text/html" encoding="UTF-8" omit-xml-declaration="yes" xml:lang="fr" xml:space="preserve"/><xsl:variable xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" name="nb-admission"><xsl:value-of select="count(key('objet',$idP)/formalPrerequisites//text())+count(key('objet',$idP)/recommendedPrerequisites//text())+count(key('program',$idP)/ancestor-or-self::program/admissions/refProgram)"/></xsl:variable><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" name="user-admission">
    <hr class="visible-print"/>
		<h3>Pré-requis</h3>
    <xsl:apply-templates select="key('objet',$mention)/formalPrerequisites" mode="screen"/>
    <xsl:for-each select="key('objet',$mention)/programStructure//refProgram[@role='parcours']">
      <div class="cb-{@ref} cb"><xsl:text> </xsl:text>
        <xsl:apply-templates select="key('objet',@ref)/formalPrerequisites" mode="screen"/>
      </div>
    </xsl:for-each>
    <h3>Admission</h3>
    <xsl:apply-templates select="key('objet',$mention)/recommendedPrerequisites" mode="screen"/>
    <xsl:for-each select="key('objet',$mention)/programStructure//refProgram[@role='parcours']">
      <div class="cb-{@ref} cb"><xsl:text> </xsl:text>
        <xsl:apply-templates select="key('objet',@ref)/recommendedPrerequisites" mode="screen"/>
      </div>
    </xsl:for-each>
    <xsl:choose>
      <xsl:when test="key('objet',$mention)/qualification/degree/@degree='licence'">
        <xsl:copy-of select="/CDM/orgUnit[1]/admission/subBlock[@userDefined='uniform.licence']"/>
        <xsl:copy-of select="/CDM/orgUnit[1]/expenses/subBlock[@userDefined='uniform.commun' or @userDefined='uniform.other']"/>
      </xsl:when>
      <xsl:when test="key('objet',$mention)/qualification/degree/@degree='dut'">
        <xsl:copy-of select="/CDM/orgUnit[1]/admission/subBlock[@userDefined='uniform.DUT']"/>
        <xsl:copy-of select="/CDM/orgUnit[1]/expenses/subBlock[@userDefined='uniform.commun' or @userDefined='uniform.DUT']"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="/CDM/orgUnit[1]/expenses/subBlock[@userDefined='uniform.commun' or @userDefined='uniform.other']"/>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="formalPrerequisites/subBlock//header"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="recommendedPrerequisites/subBlock//header"/><xsl:template xmlns="http://www.w3.org/1999/xhtml" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:xalan="http://xml.apache.org/xalan" match="expenses//header"/><!--FIN INCLUDE OF '../lille1-1.0/onglets/admission.xsl'-->
  
	<xsl:variable name="degree"><xsl:value-of select="key('objet',$mention)/qualification/degree/@degree"/></xsl:variable>
	<xsl:variable name="domain"><xsl:value-of select="key('objet',$mention)/programCode/codeDomain"/></xsl:variable>
	<xsl:variable name="listeIdentifiants"/>
	<xsl:variable name="titreSimplifie">
		<xsl:value-of select="key('objet', $mention)/programName/text[last()]"/>
	</xsl:variable>
	<xsl:variable name="max" select="key('noeudId',$mention)/@niveau"/>
	<xsl:template match="/">
		<xsl:call-template name="html"/>
	</xsl:template>
	<xsl:template name="user-titreFenetre">
		<xsl:call-template name="getVar"><xsl:with-param name="var">0</xsl:with-param></xsl:call-template>
	</xsl:template>
  <xsl:template name="page-script">
		<script>
     <xsl:text disable-output-escaping="yes">  
     $(document).ready(function() {
      $("input.cb").change(function() {  
        $(".radio label").removeClass("active");
        $(this).parent().addClass("active");
        $("li.print").text($(this).parent().text());
        $("div.cb").hide();
        $("." + this.id).show();
      });
      $("input.cb").first().click();
      });
     </xsl:text> 
    </script>
	</xsl:template>
  <xsl:template name="user-navigation">
  	<li class="menuHorizontal"><a href="{$absActionURL}/{$_lang}/liste">Feuilleter le catalogue</a></li>
  	<li class="menuHorizontal"><a href="{$absActionURL}/{$_lang}/recherche">Recherche avancée</a></li>
	</xsl:template>
	<xsl:template name="user-titre">
		<xsl:apply-templates mode="fiche" select="key('noeudId',$mention)"/>
	</xsl:template>
	<xsl:template match="noeud" mode="fiche">
		<xsl:variable name="niveauTitre"><xsl:value-of select="$max - @niveau + 1"/></xsl:variable>
		<xsl:choose>
			<xsl:when test="@niveau=1 and $niveauTitre&gt;2">
				<span class="titre titre_n{$niveauTitre}"><xsl:value-of select="@libelle"/></span><br/>
			</xsl:when>
			<xsl:when test="$niveauTitre=2">
				<span class="titre titre_n{$niveauTitre}"><xsl:value-of select="@libelle"/><xsl:text> - </xsl:text></span> 
			</xsl:when>
			<xsl:when test="$niveauTitre=1">
				<span class="titre titre_n{$niveauTitre}"><xsl:value-of select="@libelle"/></span>
			</xsl:when>
			<xsl:otherwise/>
		</xsl:choose>
		<xsl:if test="@id!=$mention"><xsl:apply-templates mode="fiche" select="noeud[.//noeud[@id=$mention] or @id=$mention]"/></xsl:if>
	</xsl:template>

	<!-- LAG : Affichage du Fil d'Ariance complet -->
	<xsl:template name="user-ariane">
     <ol class="breadcrumb">
      <li><a class="active" href="{$absActionURL}/{$_lang}/accueil/{$_ecran}-{key('objet',$mention)/qualification/degree/@degree}-{key('objet',$mention)/programCode/codeDomain[1]}"><span class="glyphicon glyphicon-home"><xsl:text> </xsl:text></span><xsl:text> Catalogue des Formations</xsl:text></a></li>
      <xsl:choose>
        <xsl:when test="$_ecran='re'"><li class="active"><a href="{$absActionURL}/{$_lang}/last">Votre recherche</a></li></xsl:when>
      </xsl:choose>
		</ol>
  </xsl:template>
  <xsl:template name="user-ariane-cms">
    <xsl:if test="$cms='o'">
      <ol class="breadcrumb">
        <li><a class="active" href="{$absActionURL}/{$_lang}/accueil/{$_ecran}-{key('objet',$mention)/qualification/degree/@degree}-{key('objet',$mention)/programCode/codeDomain[1]}"><span class="glyphicon glyphicon-home"><xsl:text> </xsl:text></span><xsl:text> Retour à l'offre de formation</xsl:text></a></li>
      </ol>
    </xsl:if>
  </xsl:template>

 	<!-- TEMPLATES A NE PAS SORTIR CAR PROPRE A LA PAGE -->
  <xsl:template name="duration">
    <xsl:choose>
      <xsl:when test="key('objet',$mention)/qualification/degree/@degree='dut'">2 ans</xsl:when>
      <xsl:when test="key('objet',$mention)/qualification/degree/@degree='DEUST'">2 ans</xsl:when>
       <xsl:when test="key('objet',$mention)/qualification/degree/@degree='licence'">3 ans</xsl:when>
       <xsl:when test="key('objet',$mention)/qualification/degree/@degree='licencePro'">1 an</xsl:when>
       <xsl:when test="key('objet',$mention)/qualification/degree/@degree='master'">2 ans</xsl:when>
       <xsl:when test="key('objet',$mention)/qualification/degree/@degree='specifique'"><xsl:value-of select="count(key('objet',$mention)/subProgram) div 2"/> an<xsl:if test="count(key('objet',$mention)/subProgram)&gt;3">s</xsl:if></xsl:when>
      <xsl:otherwise> ???</xsl:otherwise>
    </xsl:choose>
	</xsl:template>
  <xsl:template name="ects">
    <xsl:choose>
      <xsl:when test="key('objet',$mention)/qualification/degree/@degree='dut'">120</xsl:when>
      <xsl:when test="key('objet',$mention)/qualification/degree/@degree='DEUST'">120</xsl:when>
       <xsl:when test="key('objet',$mention)/qualification/degree/@degree='licence'">180</xsl:when>
       <xsl:when test="key('objet',$mention)/qualification/degree/@degree='licencePro'">60</xsl:when>
       <xsl:when test="key('objet',$mention)/qualification/degree/@degree='master'">120</xsl:when>
       <xsl:when test="key('objet',$mention)/qualification/degree/@degree='specifique'"><xsl:value-of select="count(key('objet',$mention)/subProgram)*30"/></xsl:when>
      <xsl:otherwise> ???</xsl:otherwise>
    </xsl:choose>
	</xsl:template>
	<xsl:template name="liste-parcours">
    <xsl:choose>
      <xsl:when test="count(key('objet',$mention)/programStructure/refProgram[@role='parcours'])&gt;1"><xsl:call-template name="form-parcours"/></xsl:when>
      <xsl:when test="count(key('objet',$mention)/programStructure/refProgram[@role='parcours'])=0"/>
      <xsl:otherwise><!--xsl:apply-templates select="key('objet',$mention)/programStructure/refProgram[@role='parcours']" mode="liste-parcours"/--><xsl:call-template name="form-parcours"/></xsl:otherwise>
    </xsl:choose>
	</xsl:template>
  <xsl:template match="refProgram" mode="liste-parcours">
    <xsl:if test="key('objet',@ref)"><xsl:value-of select="key('objet',@ref)/programName/text[last()]"/><xsl:if test="following-sibling::refProgram">, </xsl:if></xsl:if>
	</xsl:template>
  <xsl:template name="form-parcours">
    <xsl:if test="key('objet',@ref)"><xsl:value-of select="key('objet',@ref)/programName/text[last()]"/><xsl:if test="following-sibling::refProgram">, </xsl:if></xsl:if>
    <form class="well well-sm" id="parcours" role="form">
      <xsl:if test="count(key('objet',$mention)/programStructure/refProgram[@role='parcours'])&gt;1"><h3>Consultez un parcours en cochant la case</h3></xsl:if>
      <xsl:apply-templates mode="form-parcours" select="key('objet',$mention)/programStructure/refProgram[@role='parcours']"/>
    </form>
	</xsl:template>
  <xsl:template match="refProgram" mode="form-parcours">
    <div class="radio">
      <label><input class="cb" id="cb-{@ref}" name="parcours" type="radio"/><xsl:text> </xsl:text><xsl:value-of select="key('objet',@ref)/programName/text[last()]"/></label>
    </div>
	</xsl:template>


  <xsl:template name="user-col_droite">

	</xsl:template>
	<xsl:template name="user-col_gauche">
    <section class="col-sm-12" id="content">
    <h2 class="title-lille1"><xsl:value-of select="key('objet',$mention)/programName/text[last()]"/></h2>
      <xsl:call-template name="user-ariane-cms"/>
      <ul class="list-unstyled">
        <li><strong>Durée des études : <xsl:call-template name="duration"/></strong></li>
        <li><strong>Crédits : <xsl:call-template name="ects"/></strong></li>
        <xsl:if test="count(/CDM/program)&gt;1"><li class="screen"><strong><xsl:value-of select="count(key('objet',$mention)/programStructure/refProgram[@role='parcours'])"/> Parcours : <xsl:call-template name="liste-parcours"/></strong></li></xsl:if>
        <li class="print"><xsl:text> </xsl:text></li>
      </ul>
      <a class="btn btn-default btn-xs pull-right hidden-xs" href="javascript:print()" id="print_btn"><span class="glyphicon glyphicon-print"><xsl:text> </xsl:text></span><xsl:text> </xsl:text>Imprimer</a>
      <ul class="nav nav-tabs hidden-xs" id="tabs_menu" role="tablist">
        <li class="active"><a data-toggle="tab" href="#presentation" role="tab">Présentation</a></li>
        <li><a data-toggle="tab" href="#competences" role="tab">Compétences visées</a></li>
        <li><a data-toggle="tab" href="#programme" role="tab">Programme</a></li>
        <li><a data-toggle="tab" href="#admission" role="tab">Admission</a></li>
        <li><a data-toggle="tab" href="#poursuite" role="tab">Et après ?</a></li>
        <li><a data-toggle="tab" href="#contact" role="tab">Contacts</a></li>
      </ul>
      <form class="visible-xs" role="form">
        <label class="sr-only" for="panels-selector">Menu</label>
        <select class="form-control" id="panels-selector" name="panels-selector" onchange="changeMenu();">
          <option selected="selected" value="#presentation">Présentation</option>
          <option value="#competences">Compétences visées</option>
          <option value="#programme">Programme</option>
          <option value="#admission">Admission</option>
          <option value="#poursuite">Et après ?</option>
          <option value="#contact">Contact</option>
        </select>
      </form>
      	<!-- Tab panes -->
				<div class="tab-content col-sm-8">
					<div class="tab-pane fade in active" id="presentation">
						<xsl:call-template name="user-presentation"/>
					</div>
					<div class="tab-pane fade" id="competences">
            <xsl:call-template name="user-competences"/>
					</div>
					<div class="tab-pane fade" id="programme">
            <xsl:call-template name="user-programme"/>
					</div>
					<div class="tab-pane fade" id="admission">
            <xsl:call-template name="user-admission"/>
					</div>
					<div class="tab-pane fade" id="poursuite">
            <xsl:call-template name="user-poursuite"/>
					</div>
					<div class="tab-pane fade" id="contact">
            <xsl:call-template name="user-contacts"/>
					</div>
				</div>
       </section>
	</xsl:template>
	<xsl:template match="refProgram">
		<li>
			<a href="{$absActionURL}/{$_lang}/fiche/description/{$mention}/{key('noeudId',@ref)/@nid}">
				<xsl:call-template name="tProgramRefProgramProgramNameText"><xsl:with-param name="pRef" select="@ref"/></xsl:call-template>
			</a>
		</li>
	</xsl:template>
	<xsl:template match="infoBlock/webLink" mode="OFIP">
		<span class="dl"><xsl:apply-templates select="."/></span>
	</xsl:template>
  <xsl:template match="*" mode="screen">
    <xsl:choose>
      <xsl:when test="xhtml"><xsl:value-of disable-output-escaping="yes" select="xhtml/text()"/></xsl:when>
      <xsl:otherwise><xsl:apply-templates mode="paragraphe" select="br|ul|div|span"/></xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>

  <xsl:template match="br|ul|div|span" mode="paragraphe">
    <xsl:if test="name()='ul' or name()='div' "><xsl:copy-of select="."/></xsl:if>
    <xsl:variable name="nb" select="count(following-sibling::br)+count(following-sibling::ul)+count(following-sibling::ol)+count(following-sibling::div)"/>
    <xsl:variable name="texte"><xsl:call-template name="texte"/></xsl:variable>
    <xsl:if test="name()='span' or string-length(normalize-space($texte))&gt;0">
      <p>
        <xsl:for-each select="following-sibling::*|following-sibling::text()">
          <xsl:variable name="reste" select="count(following-sibling::br)+count(following-sibling::ul)+count(following-sibling::ol)+count(following-sibling::div)"/>
          <xsl:if test="$reste=$nb or $nb=1">
              <xsl:apply-templates mode="para" select="."/>
          </xsl:if>
        </xsl:for-each>
      </p>
    </xsl:if>
  </xsl:template>
  <xsl:template name="texte">
    <xsl:variable name="nb" select="count(following-sibling::br)+count(following-sibling::ul)+count(following-sibling::ol)+count(following-sibling::div)"/>
    <xsl:for-each select="following-sibling::*|following-sibling::text()">
        <xsl:variable name="reste" select="count(following-sibling::br)+count(following-sibling::ul)+count(following-sibling::ol)+count(following-sibling::div)"/>
        <xsl:if test="$reste=$nb or $nb=1">
            <xsl:if test="self::text()"><xsl:copy-of select="."/></xsl:if>
            <xsl:copy-of select=".//text()"/>
        </xsl:if>
      </xsl:for-each>
  </xsl:template>
  <xsl:template match="b|i|u" mode="para">
    <xsl:if test="text()"><xsl:copy-of select="."/></xsl:if>
  </xsl:template>
  
  <xsl:template match="ul|li|a|span|font|div|text()" mode="para">
    <xsl:copy-of select="."/>
  </xsl:template>
  
  <xsl:template name="user-stat">				<!-- Piwik --> 
<script type="text/javascript">
  var domaine = "<xsl:value-of select="key('objet',$mention)/programCode/codeDomain[1]"/>" ;
  var diplome = "<xsl:value-of select="key('objet',$mention)/qualification/degree/@degree"/>" ;
  var fiche  = "<xsl:value-of select="key('objet',$mention)/programName/text"/>" ;
  
  var _paq = _paq || [];
  (function(){ var u=(("https:" == document.location.protocol) ? "https://webstat.univ-lille1.fr/" : "http://webstat.univ-lille1.fr/");
  _paq.push(['setSiteId', 5]);
  _paq.push(['setTrackerUrl', u+'piwik.php']);
  _paq.push(['setCustomVariable','1','catalogue',fiche, 'page']);
  _paq.push(['setCustomVariable','2','domaine',domaine, 'page']);
  _paq.push(['setCustomVariable','3','diplome',diplome, 'page']);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript'; g.defer=true; g.async=true; g.src=u+'piwik.js';
  s.parentNode.insertBefore(g,s); })();
</script><noscript><p><img alt="" src="http://webstat.univ-lille1.fr/piwik.php?idsite=5" style="border:0"/></p></noscript>
<!-- End Piwik Tracking Code -->
</xsl:template>
<xsl:template name="user-stat-cms">
  <script type="text/javascript">
  var titreCatalogue="<xsl:value-of select="$titreCatalogue"/>";
  var res = ""+screen.width+"x"+screen.height ;
  var now = Date.now() ;
  var _url = "<xsl:value-of select="$url"/>"+"/<xsl:value-of select="$_lang"/>/fiche/--"+"<xsl:value-of select="key('objet',$mention)/@id"/>";
  var cvar = '{"1":["catalogue","fiche"],"2":["domaine","<xsl:value-of select="key('objet',$mention)/programCode/codeDomain[1]"/>"],"3":["diplome","<xsl:value-of select="key('objet',$mention)/qualification/degree/@degree"/>"],"4":["fiche","<xsl:value-of select="key('objet',$mention)/programName/text"/>"]}';
  var base = (("https:" == document.location.protocol) ? "https://webstat.univ-lille1.fr/" : "http://webstat.univ-lille1.fr/");
 <xsl:text disable-output-escaping="yes">
 var theUrl = encodeURI(base+"piwik.php?idsite=5&amp;rec=1&amp;send_image=0&amp;action_name="+titreCatalogue+"&amp;cvar="+cvar+"&amp;url="+_url+"&amp;res="+res+"&amp;urlref="+document.referrer+"&amp;now="+now);

</xsl:text>
  var xmlHttp = null;
  xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", theUrl, false );
  xmlHttp.setRequestHeader("Content-Type", "image/gif");
  xmlHttp.send( null );
</script>
</xsl:template>
</xsl:stylesheet>