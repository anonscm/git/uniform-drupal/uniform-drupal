function encodeUTF8(string) {   
        string = string.replace(/\r\n/g,"\n");   
        var utftext = "";   
  
        for (var n = 0; n < string.length; n++) {   
  
            var c = string.charCodeAt(n);   
  
            if (c < 128) {   
                utftext += String.fromCharCode(c);   
            }   
            else if((c > 127) && (c < 2048)) {   
                utftext += String.fromCharCode((c >> 6) | 192);   
                utftext += String.fromCharCode((c & 63) | 128);   
            }   
            else {   
                utftext += String.fromCharCode((c >> 12) | 224);   
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);   
                utftext += String.fromCharCode((c & 63) | 128);   
            }   
  
        }   
  
        return utftext;   
}


sfHover = function() {
	var sfEls = document.getElementById("nav").getElementsByTagName("LI");
	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" sfhover";
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);
var anchorOpen="<a href=\"\">";
var anchorClose="</a>";