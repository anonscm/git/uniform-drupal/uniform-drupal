
$(document).ready(function() {
  $("input.cb").change(function() {  
    $(".radio label").removeClass("active");
    $(this).parent().addClass("active");
    $("li.print").text($(this).parent().text());
    $("div.cb").hide();
    $("." + this.id).show();
  });
  $("input.cb").first().click();
});